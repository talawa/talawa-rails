# frozen_string_literal: true

require "sidekiq/web"

Rails.application.routes.draw do
  require "sidekiq_unique_jobs/web"
  mount Sidekiq::Web => "/sidekiq"

  namespace :admin do
    resources :audios
    resources :users

    root to: "users#index"
  end
  get "up" => "rails/health#show", as: :rails_health_check

  draw(:users)

  root "audios/audios#feed"
end
