# frozen_string_literal: true

Rails.application.configure do
  # Load all default .env.template ENV key into Rails.application.credentials
  Dotenv.parse(".env.template").each_key do |key|
    # Using ENV.fetch without default value here will raise a KeyNotFound exception
    credentials.send(:"#{key.downcase}=", ENV.fetch(key))
  end
end
