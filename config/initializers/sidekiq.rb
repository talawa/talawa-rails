# frozen_string_literal: true

require "sidekiq-unique-jobs"

Sidekiq.configure_server do |config|
  config.capsule("convert") do |cap|
    cap.concurrency = Rails.env.development? ? 3 : 1
    cap.queues = %w[convert]
  end

  config.client_middleware do |chain|
    chain.add SidekiqUniqueJobs::Middleware::Client
  end

  config.server_middleware do |chain|
    chain.add SidekiqUniqueJobs::Middleware::Server
  end

  config.redis = { url: Rails.configuration.credentials.redis_url, network_timeout: 10 }

  SidekiqUniqueJobs::Server.configure(config)
end

Sidekiq.configure_client do |config|
  config.redis = { url: Rails.configuration.credentials.redis_url, network_timeout: 10 }

  config.client_middleware do |chain|
    chain.add SidekiqUniqueJobs::Middleware::Client
  end
end
