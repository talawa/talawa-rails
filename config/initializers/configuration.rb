# frozen_string_literal: true

Rails.application.config.tags = { max_count: 5, min_length: 3, max_length: 65 }
Rails.application.config.comments = { min_length: 3 }
Rails.application.config.titles = { min_length: 3, max_length: 30 }
