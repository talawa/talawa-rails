# frozen_string_literal: true

require "active_storage/analyzer/talawa_analyza"

Rails.application.config.active_storage.routes_prefix = "/files"
Rails.application.config.active_storage.analyzers = [ActiveStorage::Analyzer::TalawaAnalyza]
