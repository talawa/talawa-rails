# frozen_string_literal: true

require "ffmpeg"

FFMPEG.ffmpeg_binary = Rails.root.join("bin/nice_ffmpeg").to_s
FFMPEG.logger = Logger.new("/dev/null")
FFMPEG::Transcoder.timeout = nil
