# frozen_string_literal: true

# CREATE TABLE `uploads_comments` (
#   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#   `upload_id` int(11) unsigned NOT NULL,
#   `user_id` int(11) unsigned NOT NULL,
#   `body` mediumtext NOT NULL,
#   `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   `disabled` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   PRIMARY KEY (`id`),
#   KEY `upload_id` (`upload_id`),
#   KEY `user_id` (`user_id`)
# ) ENGINE=MyISAM AUTO_INCREMENT=43898 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class UploadComment < BaseRecord
    self.table_name = "uploads_comments"

    belongs_to :upload

    default_scope { where("`uploads_comments`.`disabled` = '0'") }
    scope :audio, -> { joins(:upload).merge(Upload.audio) }
  end
end
