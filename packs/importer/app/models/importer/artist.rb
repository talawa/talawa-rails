# frozen_string_literal: true

# CREATE TABLE `artists` (
#   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#   `name` varchar(255) DEFAULT NULL,
#   `user_id` int(11) unsigned DEFAULT NULL,
#   `disabled` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   PRIMARY KEY (`id`),
#   KEY `artists_name_index` (`name`(250)),
#   KEY `artists_user_id_index` (`user_id`),
#   KEY `artists_disabled_index` (`disabled`),
#   FULLTEXT KEY `artists_fulltext` (`name`)
# ) ENGINE=MyISAM AUTO_INCREMENT=24648 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class Artist < BaseRecord
    has_many :uploads, class_name: "Importer::Upload"
  end
end
