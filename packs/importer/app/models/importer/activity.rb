# frozen_string_literal: true

# CREATE TABLE `activities` (
#   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#   `user_id` int(11) unsigned NOT NULL,
#   `activity_id` int(11) unsigned NOT NULL,
#   `linked_id` int(11) unsigned NOT NULL DEFAULT 0,
#   `specific_id` int(11) unsigned NOT NULL DEFAULT 0,
#   `activity_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   PRIMARY KEY (`id`),
#   KEY `user_id` (`user_id`),
#   KEY `activity_id` (`activity_id`),
#   KEY `linked_id` (`linked_id`),
#   KEY `activities_specific_id_index` (`specific_id`),
#   KEY `activities_activity_date_index` (`activity_date`)
# ) ENGINE=MyISAM AUTO_INCREMENT=136421 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class Activity < BaseRecord
    has_one :user, class_name: "Importer::User", foreign_key: "id", primary_key: :linked_id
    has_one :upload, class_name: "Importer::Upload", foreign_key: "id", primary_key: :linked_id
    has_one :comment, class_name: "Importer::UploadComment", foreign_key: "id", primary_key: :specific_id
    has_one :file, class_name: "Importer::UploadFile", foreign_key: "id", primary_key: :linked_id

    enum :activity_id, { user: 1, upload: 2, comment: 4, file: 9 }

    scope :importable, -> { where(activity_id: activity_ids.values) }

    def assoc
      send(activity_id)
    end
  end
end
