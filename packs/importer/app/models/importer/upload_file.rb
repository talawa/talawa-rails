# frozen_string_literal: true

module Importer
  class UploadFile < BaseRecord
    self.table_name = "uploads_files"

    belongs_to :user
    belongs_to :upload

    scope :audio, -> { joins(:upload).merge(Upload.audio) }
    scope :not_converted, -> { where("converted = '0'") }

    def path
      if upload.audio?
        "#{Rails.application.credentials.old_website_data_path}/#{user.user_key}/medias/#{unique_id}.m4a"
      else
        "#{Rails.application.credentials.old_website_data_path}/#{user.user_key}/medias/#{unique_id}"
      end
    end
  end
end
