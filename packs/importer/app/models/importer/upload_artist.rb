# frozen_string_literal: true

# CREATE TABLE `uploads_artists` (
#   `upload_id` int(11) unsigned NOT NULL,
#   `artist_id` int(11) unsigned NOT NULL,
#   `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   KEY `artist_id` (`artist_id`),
#   KEY `upload_id` (`upload_id`)
# ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class UploadArtist < BaseRecord
    self.table_name = "uploads_artists"

    belongs_to :upload
    belongs_to :artist
    has_many :files, through: :upload

    scope :audio, -> { joins(:upload).merge(Upload.audio) }
  end
end
