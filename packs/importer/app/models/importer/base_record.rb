# frozen_string_literal: true

module Importer
  class BaseRecord < ApplicationRecord
    self.abstract_class = true

    connects_to database: { reading: :talawa_prod, writing: :talawa_prod }
  end
end
