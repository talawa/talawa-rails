# frozen_string_literal: true

# CREATE TABLE `uploads` (
#   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#   `name` varchar(65) NOT NULL,
#   `short_name` varchar(255) NOT NULL,
#   `unique_id` varchar(32) NOT NULL,
#   `web_id` varchar(5) NOT NULL,
#   `date` date NOT NULL DEFAULT '0000-00-00',
#   `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   `user_id` int(11) unsigned NOT NULL,
#   `category_id` int(11) unsigned NOT NULL,
#   `sub_category_id` int(11) unsigned NOT NULL,
#   `info` mediumtext DEFAULT NULL,
#   `tracklist` mediumtext DEFAULT NULL,
#   `hits` int(11) unsigned NOT NULL DEFAULT 0,
#   `downloadable` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '1',
#   `disabled` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `web_id` (`web_id`),
#   KEY `user_id` (`user_id`),
#   KEY `sub_category_id` (`sub_category_id`),
#   KEY `category_id` (`category_id`),
#   KEY `uploads_create_date_index` (`create_date`),
#   KEY `uploads_hits_index` (`hits`),
#   KEY `uploads_disabled_index` (`disabled`),
#   KEY `uploads_short_name_index` (`short_name`(250)),
#   FULLTEXT KEY `uploads_fulltext` (`name`,`info`,`tracklist`)
# ) ENGINE=MyISAM AUTO_INCREMENT=41246 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class Upload < BaseRecord
    belongs_to :user
    has_many :files, class_name: "Importer::UploadFile"
    has_many :upload_artists, class_name: "Importer::UploadArtist"
    has_many :artists, through: :upload_artists
    has_many :comments, class_name: "Importer::UploadComment"

    enum :category_id, { audio: 1, video: 2, picture: 3, other: 4 }
    enum :sub_category_id, { radio_show: 1, live_session: 2, mix_tape: 3, production: 4 }

    scope :with_one_file, -> { left_joins(:files).group("uploads_files.upload_id").having("COUNT(uploads_files.id)=1") }
    scope :with_many_files, -> { left_joins(:files).group("uploads_files.upload_id").having("COUNT(uploads_files.upload_id)>1") }
    scope :with_files, -> { left_joins(:files).group("uploads_files.upload_id").having("COUNT(uploads_files.upload_id)>=1") }
    scope :with_no_file, -> { left_joins(:files).group("uploads_files.upload_id").having("COUNT(uploads_files.upload_id)=0") }

    def audios
      ::Audio.where("(settings ->> 'upload_id')::int = ?", id)
    end

    def many_files?
      files.to_a.size > 1
    end

    # If the average length of tracks is short (less than 34 min), we can merge all tracks into one file
    def can_be_merged?
      audio? && many_files? && (files.to_a.pluck(:length).sum / files.to_a.size) <= 2000
    end

    def tags(uploads_artists = nil, import_artists = nil) # rubocop:disable Metrics/CyclomaticComplexity
      @tags ||= begin
        uploads_artists ||= Importer::UploadArtist.audio.where.associated(:artist).group(:artist_id).count.select { |_k, v| v > 1 }
        import_artists ||= Importer::Artist.pluck(:id, :name).to_h

        # Take the most five popular artists
        artist_ids = artists.pluck(:id).select { |id| uploads_artists[id] }.sort_by { |id| -uploads_artists[id] }[..4]
        artist_ids.map { |id| import_artists[id] }
      end
    end

    def cover
      "#{Rails.application.credentials.old_website_data_path}/#{user.user_key}/covers/#{unique_id}"
    end
  end
end
