# frozen_string_literal: true

# CREATE TABLE `users_favorites` (
#   `user_id` int(11) unsigned NOT NULL,
#   `upload_id` int(11) NOT NULL,
#   KEY `upload_id` (`upload_id`),
#   KEY `user_id` (`user_id`)
# ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class UserFavorite < BaseRecord
    self.table_name = "users_favorites"

    belongs_to :user
    belongs_to :upload

    scope :audio, -> { joins(:upload).merge(Upload.audio) }
  end
end
