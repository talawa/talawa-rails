# frozen_string_literal: true

# CREATE TABLE `users` (
#   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#   `name` varchar(30) NOT NULL,
#   `password` varchar(40) NOT NULL,
#   `secret` varchar(32) NOT NULL,
#   `mail` varchar(250) NOT NULL,
#   `user_key` varchar(32) NOT NULL,
#   `reset_key` varchar(32) DEFAULT NULL,
#   `reset_expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   `user_class` int(11) unsigned DEFAULT NULL,
#   `admin` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   `moderator` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   `join_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#   `first_visit` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '1',
#   `hits` int(11) unsigned NOT NULL DEFAULT 0,
#   `ip` varchar(16) DEFAULT NULL,
#   `invalid_mail` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   `disabled` enum('0','1') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `name` (`name`),
#   UNIQUE KEY `mail` (`mail`),
#   UNIQUE KEY `user_key` (`user_key`)
# ) ENGINE=MyISAM AUTO_INCREMENT=20158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
module Importer
  class User < BaseRecord
    has_many :files, class_name: "Importer::UploadFile"
    has_many :uploads, class_name: "Importer::Upload"
    has_one :avatar, class_name: "Importer::UserAvatar"
    has_one :info, class_name: "Importer::UserInfo"
  end
end
