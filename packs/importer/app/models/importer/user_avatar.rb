# frozen_string_literal: true

module Importer
  class UserAvatar < BaseRecord
    self.table_name = "users_avatars"
    default_scope { where("`users_avatars`.`actif` = '1'") }

    belongs_to :user

    def path
      "#{Rails.application.credentials.old_website_data_path}/#{user.user_key}/avatars/#{unique_id}"
    end
  end
end
