# frozen_string_literal: true

module Importer
  class UserInfo < BaseRecord
    self.table_name = "users_infos"

    belongs_to :user
  end
end
