# frozen_string_literal: true

module Importer
  class AttachFileJob < Job
    sidekiq_options retry: 0,
                    lock: :until_and_while_executing,
                    on_conflict: { client: :log, server: :reject }

    def perform(audio_id, file_path)
      audio = Audio.find(audio_id)

      Importer::AttachFileService.perform(audio, file_path)
    end
  end
end
