# frozen_string_literal: true

module Importer
  class ScanActivitiesTableJob < Job
    sidekiq_options retry: 0,
                    lock: :until_and_while_executing,
                    on_conflict: { client: :log, server: :reject }

    def perform(last_id)
      new_id = Importer::ScanActivitiesTableService.perform(last_id)
      last_id = new_id if new_id.present?
    ensure
      Importer::ScanActivitiesTableJob.perform_in(1.minute, last_id)
    end
  end
end
