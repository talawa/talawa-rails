# frozen_string_literal: true

module Importer
  class ImportActivityJob < Job
    sidekiq_options retry: 5,
                    lock: :until_executed

    def perform(activity_id)
      activity = Importer::Activity.find(activity_id)

      if activity.file?
        Importer::ImportFileService.perform(activity)
      else
        Importer::ImportActivityService.perform(activity)
      end
    end
  end
end
