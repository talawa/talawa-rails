# frozen_string_literal: true

module Importer
  class ArchiveUploadJob < Job
    sidekiq_options queue: :convert,
                    retry: 0,
                    lock: :until_and_while_executing,
                    on_conflict: { client: :log, server: :reject }

    def perform(upload_id)
      upload = Importer::Upload.not_audio.includes(:files).find(upload_id)

      Importer::ArchiveUploadService.perform(upload)
    end
  end
end
