# frozen_string_literal: true

module Importer
  class ArchiveUploadService < Service
    attr_reader :upload

    def initialize(upload)
      @upload = upload
    end

    def perform
      require "fog"
      Fog::Logger[:deprecation] = nil
      Fog::Logger[:warning] = nil

      upload.files.map do |file|
        next if Rails.env.development? && !File.exist?(file.path)

        directory.files.create(
          key: "#{upload.short_name}-#{file.short_name}.#{file.extension}",
          body: File.open(file.path),
        )
      end
    end

    private

    def client
      @client ||= ::Fog::Storage::InternetArchive.new(
        ia_access_key_id: Rails.application.credentials.internet_archive_access_key,
        ia_secret_access_key: Rails.application.credentials.internet_archive_secret_key,
      )
    end

    def directory
      @directory ||= client.directories.get(directory_key)
    end

    def directory_key
      key = if upload.video?
              "talawa-videos-archive"
            elsif upload.picture?
              "talawa-images-archive"
            else
              "talawa-texts-archive"
            end
      if Rails.env.development?
        "#{key}-development"
      else
        key
      end
    end
  end
end
