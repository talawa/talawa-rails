# frozen_string_literal: true

module Importer
  class MergeUploadService < Service
    attr_reader :audio, :files

    def initialize(audio, files)
      @audio = audio
      @files = files
    end

    def perform
      return if Rails.env.development? && files.any? { |file| !File.exist?(file) }

      concat
      attach
    ensure
      tmp_files.each_value do |file|
        unlink = true
        file.close(unlink)
      end
    end

    private

    def concat
      output_file = tmp_files("concat")
      cmd = "#{FFMPEG.ffmpeg_binary} -y #{input_files} -filter_complex #{concat_opts} -map '[out]' -c:a libfdk_aac -vbr 4 #{output_file.path}"
      Rails.logger.info cmd
      # Can't use FFMPEG lib here...
      ret = system(cmd)
      raise StandardError, "Error concatenating files for #{audio.id}" unless ret
    end

    def attach
      file = tmp_files("concat")
      Importer::AttachFileService.perform(audio, file.path, force: true)
    end

    def input_files
      files.map { |file| "-i #{file}" }.join(" ")
    end

    def concat_opts
      opts = files.each_index.map { |idx| "[#{idx}:0]" }.join
      "'#{opts}concat=n=#{files.count}:v=0:a=1[out]'"
    end

    def tmp_files(name = nil)
      @tmp_files ||= {}
      return @tmp_files unless name

      @tmp_files[name] ||= Tempfile.new(%W[#{Rails.env}-#{name}-#{audio.id}- .m4a], Rails.root.join("tmp"))
    end
  end
end
