# frozen_string_literal: true

module Importer
  class AttachFileService < Service
    attr_reader :audio, :file, :force

    def initialize(audio, file, force: false)
      @audio = audio
      @file = file
      @force = force
    end

    def perform
      return if Rails.env.development? && !File.exist?(file)

      audio.file.attach(
        io: File.open(file),
        filename: "#{audio.slug}.m4a",
        metadata: { analyzed: true }, # Analyze of the file will be done in the transcode job, skip it for now
      )

      Audios::TranscodeFileJob.perform_async(audio.id, force)
    end
  end
end
