# frozen_string_literal: true

module Importer
  class ImportActivityService < Service
    attr_reader :activity

    def initialize(activity)
      @activity = activity
    end

    def perform
      if activity.upload?
        import_upload
      else
        attributes = Importer::Attributes.for(type:, record:)
        class_name.upsert(attributes) # rubocop:disable Rails/SkipsModelValidations
      end
    end

    private

    def class_name
      type.classify.constantize
    end

    def record
      activity.assoc
    end

    def type
      @type ||= activity.activity_id
    end

    def import_upload
      if record.audio?
        import_audio
      else
        Importer::ArchiveUploadJob.perform_async(record.id)
      end
    end

    def import_audio
      # Still an issue with uploads without any files...
      return if record.files.empty?

      if record.files.not_converted.present?
        Importer::ImportActivityJob.perform_in(20.minutes, activity.id)
      else
        audio_ids = Audio.upsert_all(audios_attributes, returning: [:id]) # rubocop:disable Rails/SkipsModelValidations
        attach_files(audio_ids.pluck("id"))
        attach_images(audio_ids.pluck("id"))
      end
    end

    def audios_attributes
      if record.can_be_merged?
        file = record.files.first
        [Importer::Attributes.for(type: "audio", record:, file:)]
      else
        record.files.map do |file|
          title = "#{record.name} - #{file.name}"
          slug = "#{record.short_name}-#{file.short_name}-#{file.web_id}"

          Importer::Attributes.for(type: "audio", record:, file:, title:, slug:)
        end
      end
    end

    def attach_files(audio_ids)
      Audio.where(id: audio_ids).find_each do |audio|
        files = Importer::UploadFile.audio.order(:upload_id, :name).includes(:user, :upload).where(upload_id: audio.settings["upload_id"])

        if audio.settings["can_be_merged"]
          paths = files.map(&:path)
          Importer::MergeUploadJob.perform_async(audio.id, paths)
        else
          path = files.find { |f| f.id == audio.old_id }.path
          Importer::AttachFileJob.perform_async(audio.id, path)
        end
      end
    end

    def attach_images(audio_ids)
      Audio.where(id: audio_ids).find_each do |audio|
        cover = audio.settings["cover"]
        next unless cover.present? && File.exist?(cover)

        image = File.open(cover)
        audio.image.attach(io: image, filename: "image-#{audio.slug}.jpg", content_type: "image/jpeg", identify: false, metadata: { identified: true, analyzed: true })
      end
    end
  end
end
