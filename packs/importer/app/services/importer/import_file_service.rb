# frozen_string_literal: true

module Importer
  class ImportFileService < Service
    attr_reader :activity, :record

    def initialize(activity)
      @activity = activity
      @record = activity.assoc.upload
    end

    def perform
      if record.can_be_merged?
        audio = ::Audio.find_by!("(settings ->> 'upload_id')::int = ?", record.id)
        paths = record.files.map(&:path)
        Importer::MergeUploadJob.perform_async(audio.id, paths)
      else
        file = activity.assoc
        title = "#{record.name} - #{file.name}"
        slug = "#{record.short_name}-#{file.short_name}-#{file.web_id}"

        attributes = Importer::Attributes.for(type: "audio", record:, file:, title:, slug:)
        audio = Audio.create!(attributes)
        attach_cover(audio)

        Importer::AttachFileJob.perform_async(audio.id, file.path)
      end
    end

    private

    def attach_cover(audio)
      cover = audio.settings["cover"]
      return unless cover.present? && File.exist?(cover)

      image = File.open(cover)
      audio.image.attach(io: image, filename: "image-#{audio.slug}.jpg", content_type: "image/jpeg", identify: false, metadata: { identified: true, analyzed: true })
    end
  end
end
