# frozen_string_literal: true

module Importer
  class ScanActivitiesTableService < Service
    attr_reader :last_id

    def initialize(last_id)
      @last_id = last_id
    end

    def perform
      activities_ids = Importer::Activity.importable.where("id > ?", last_id).ids

      activities_ids.each do |activity_id|
        Importer::ImportActivityJob.perform_async(activity_id)
      end

      # Return last imported activity
      activities_ids.last
    end
  end
end
