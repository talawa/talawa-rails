# frozen_string_literal: true

module Importer
  class Attributes
    class Audio
      attr_accessor :record, :user_id, :tags, :file, :title, :slug

      def initialize(record, file:, title: nil, slug: nil, user_id: nil, tags: nil) # rubocop:disable Metrics/ParameterLists
        @record = record
        @file = file
        @title = title.presence || record.name
        @slug = slug.presence || "#{record.short_name}-#{file.web_id}"
        @user_id = user_id.presence || ::User.find_by!(old_id: record.user_id).id
        @tags = tags.presence || record.tags
      end

      def to_h
        {
          user_id: user_id,
          old_id: file.id,
          title: title.strip,
          slug: slug,
          description: description,
          created_at: record.create_date,
          settings: audio_settings,
          converted: false,
          disabled: record.disabled == "1",
          tags: tags,
        }
      end

      private

      def description
        if record.tracklist.blank?
          record.info
        elsif record.info.blank?
          "Tracklist:\r\n#{record.tracklist}"
        else
          "#{record.info}\r\n\r\nTracklist:\r\n#{record.tracklist}"
        end
      end

      def audio_settings
        {
          date: record.date,
          hits: record.hits,
          downloadable: record.downloadable == "1",
          web_id: record.web_id,
          upload_id: record.id,
          can_be_merged: record.can_be_merged?,
          cover: record.cover,
        }
      end
    end
  end
end
