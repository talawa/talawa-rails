# frozen_string_literal: true

module Importer
  class Attributes
    class User
      attr_accessor :record

      def initialize(record)
        @record = record
      end

      def to_h # rubocop:disable Metrics/AbcSize
        {
          old_id: record.id,
          title: record.name.strip,
          slug: record.name.parameterize(preserve_case: true), # Force slug for usernames
          email: record.mail.downcase, # Sanitize email case
          role: user_role,
          created_at: record.join_date,
          current_sign_in_at: record.last_login,
          last_sign_in_at: record.last_login,
          settings: user_settings,
          current_sign_in_ip: record.ip,
          last_sign_in_ip: record.ip,
          disabled: record.disabled == "1",
          encrypted_password: SecureRandom.uuid, # Generate a random password for imported user
          location: record.info&.location&.strip,
          description: description&.strip,
        }
      end

      private

      def user_role
        if record.moderator == "1"
          :moderator
        elsif record.admin == "1"
          :admin
        else
          :user
        end
      end

      def user_settings
        {
          first_visit: record.first_visit == "1",
          hits: record.hits,
          invalid_mail: record.invalid_mail == "1",
          old_password: record.password,
          old_secret: record.secret,
          old_user_key: record.user_key,
          old_password_migrated: false,
          imported_user: true,
        }
      end

      def description
        description = record.info&.infos

        if record.info.present?
          attrs = %w[website facebook twitter]
          websites = attrs.filter_map { |attr| website(attr) }.join("\r\n")

          if websites.present?
            if description.blank?
              description = "More infos at:\r\n#{websites}"
            else
              "#{description}\r\n\r\nMore infos at:\r\n#{websites}"
            end
          end
        end

        description
      end

      def website(name)
        value = record.info[name]
        return if value.blank?

        require "uri"

        begin
          uri = URI.parse(value)
          return if uri.host.blank?
        rescue URI::InvalidURIError
          return
        end

        "- #{value}"
      end
    end
  end
end
