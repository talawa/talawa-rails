# frozen_string_literal: true

module Importer
  class Attributes
    class Comment
      attr_accessor :record, :audio_id, :user_id

      def initialize(record, audio_id: nil, user_id: nil)
        @record = record
        @audio_id = audio_id.presence || ::Audio.find_by!("(settings ->> 'upload_id')::int = ?", record.upload_id).id
        @user_id = user_id.presence || ::User.find_by!(old_id: record.user_id).id
      end

      def to_h
        {
          user_id: user_id,
          audio_id: audio_id,
          body: record.body,
          created_at: record.date,
          updated_at: record.date,
        }
      end
    end
  end
end
