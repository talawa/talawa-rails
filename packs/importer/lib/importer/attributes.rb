# frozen_string_literal: true

module Importer
  class Attributes
    def self.for(type:, record:, **)
      "#{self}::#{type.classify}".constantize.new(record, **).to_h
    end
  end
end
