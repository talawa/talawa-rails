# frozen_string_literal: true

module Importer
  require "importer/attributes"
  require "importer/attributes/audio"
  require "importer/attributes/comment"
  require "importer/attributes/user"
end
