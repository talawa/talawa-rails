# frozen_string_literal: true

namespace :import do
  desc "Import data from old Talawa website"

  task all: [:scan, :users, :avatars, :uploads, :covers, :files, :comments, :likes, :playlists, :archive]
  task migrate: [:avatars, :likes, :playlists]

  task reset: :environment do
    %w[db:drop db:create db:migrate import:users import:avatars import:uploads import:covers import:comments import:likes import:playlists import:scan].each do |task|
      Rake::Task[task].invoke
    end
  end

  task scan: :environment do
    last_id = Importer::Activity.last.id
    Importer::ScanActivitiesTableJob.perform_async(last_id)
  end

  task users: :environment do
    data = Importer::User.includes(:info).to_a
    bar = import_task_progressbar("users", data.count)
    users_attributes = data.map do |imported_user|
      bar.advance

      Importer::Attributes.for(type: "user", record: imported_user)
    end

    User.upsert_all(users_attributes, unique_by: :old_id) # rubocop:disable Rails/SkipsModelValidations
  end

  task avatars: :environment do
    ActiveStorage::LogSubscriber.detach_from(:active_storage)
    Sidekiq.logger.level = Logger::WARN
    ActiveJob::Base.logger.level = Logger::WARN

    data = Importer::User.joins(:avatar).includes(:avatar).to_a
    bar = import_task_progressbar("avatars", data.count)
    data.each do |imported_user|
      bar.advance

      user = User.find_by(old_id: imported_user.id)
      avatar = imported_user.avatar.path
      next unless avatar.present? && File.exist?(avatar)

      image = File.open(avatar)
      user.image.attach(io: image, filename: "avatar-#{user.slug}.jpg", content_type: "image/jpeg", identify: false, metadata: { identified: true, analyzed: true })
    end
  end

  task uploads: :environment do
    data = Importer::Upload.audio.with_files.includes(:user, :files, :artists).to_a
    bar = import_task_progressbar("uploads", data.count)
    users_ids = User.pluck(:old_id, :id).to_h
    artists = Importer::Artist.pluck(:id, :name).to_h

    # Take only artists with at least 2 uploads linked. This will remove bad quality tags.
    uploads_artists = Importer::UploadArtist.audio.where.associated(:artist).group(:artist_id).count.select { |_k, v| v > 1 }

    audios_attributes = data.map do |upload|
      bar.advance

      tags = upload.tags(uploads_artists, artists)

      if upload.many_files? && !upload.can_be_merged?
        upload.files.map do |file|
          title = "#{upload.name} - #{file.name}"
          slug = "#{upload.short_name}-#{file.short_name}-#{file.web_id}"

          Importer::Attributes.for(type: "audio", record: upload, file:, title:, slug:, user_id: users_ids[upload.user_id], tags:)
        end
      else
        Importer::Attributes.for(type: "audio", record: upload, file: upload.files.first, user_id: users_ids[upload.user_id], tags:)
      end
    end.flatten

    Audio.upsert_all(audios_attributes, unique_by: :old_id) # rubocop:disable Rails/SkipsModelValidations
  end

  task covers: :environment do
    ActiveStorage::LogSubscriber.detach_from(:active_storage)
    Sidekiq.logger.level = Logger::WARN
    ActiveJob::Base.logger.level = Logger::WARN

    data = Audio.all.to_a
    bar = import_task_progressbar("covers", data.count)
    data.each do |audio|
      bar.advance

      cover = audio.settings["cover"]
      next unless cover.present? && File.exist?(cover)

      image = File.open(cover)
      audio.image.attach(io: image, filename: "image-#{audio.slug}.jpg", content_type: "image/jpeg", identify: false, metadata: { identified: true, analyzed: true })
    end
  end

  task files: :environment do
    data = Audio.all.to_a
    files = Importer::UploadFile.audio.order(:upload_id, :name).includes(:user, :upload).to_a
    bar = import_task_progressbar("files", data.count)

    job_args = data.filter_map do |audio|
      bar.advance
      if audio.settings["can_be_merged"]
        paths = files.select { |f| f.upload_id == audio.settings["upload_id"] }.map(&:path)
        Importer::MergeUploadJob.perform_async(audio.id, paths)
        nil
      else
        path = files.find { |f| f.id == audio.old_id }.path
        [audio.id, path]
      end
    end

    Sidekiq.logger.level = Logger::WARN
    Importer::AttachFileJob.perform_bulk(job_args)
  end

  task comments: :environment do
    data = Importer::UploadComment.audio.where("LENGTH(body) >= 3").to_a
    bar = import_task_progressbar("comments", data.count)
    audios_ids = Audio.pluck(Arel.sql("(settings ->> 'upload_id')::int as upload_id"), :id).group_by(&:shift).transform_values(&:flatten)
    users_ids = User.pluck(:old_id, :id).to_h

    comments_attributes = data.map do |comment|
      bar.advance

      audios_ids[comment.upload_id]&.map do |audio_id|
        Importer::Attributes.for(type: "comment", record: comment, audio_id:, user_id: users_ids[comment.user_id])
      end
    end.flatten.compact

    Comment.delete_all
    Comment.upsert_all(comments_attributes) # rubocop:disable Rails/SkipsModelValidations
    ActiveRecord::Base.connection.execute <<-SQL.squish
      UPDATE audios
      SET comments_count = (SELECT count(1) FROM comments WHERE comments.audio_id = audios.id)
    SQL
  end

  task likes: :environment do
    data = Importer::UserFavorite.audio.to_a
    bar = import_task_progressbar("likes", data.count)
    audios_ids = Audio.pluck(Arel.sql("(settings ->> 'upload_id')::int as upload_id"), :id).group_by(&:shift).transform_values(&:flatten)
    users_ids = User.pluck(:old_id, :id).to_h

    likes_attributes = data.map do |like|
      bar.advance

      audios_ids[like.upload_id]&.map do |audio_id|
        {
          user_id: users_ids[like.user_id],
          audio_id: audio_id,
        }
      end
    end.flatten.compact

    Like.delete_all
    Like.upsert_all(likes_attributes, unique_by: [:user_id, :audio_id]) # rubocop:disable Rails/SkipsModelValidations
    ActiveRecord::Base.connection.execute <<-SQL.squish
      UPDATE audios
      SET likes_count = (SELECT count(1) FROM likes WHERE likes.audio_id = audios.id)
    SQL
  end

  task playlists: :environment do
    data = Like.includes(:user, :audio).to_a
    bar = import_task_progressbar("playlists", data.count)
    title = I18n.t("default_playlist")

    playlists_attributes = data.pluck(:user_id).uniq.map do |user_id|
      {
        user_id: user_id,
        title: title,
        slug: title.parameterize,
        visibility: Playlist.visibilities[:open],
        locked: true,
      }
    end

    Playlist.delete_all
    Playlist.upsert_all(playlists_attributes) # rubocop:disable Rails/SkipsModelValidations

    playlists = Playlist.pluck(:user_id, :id).to_h
    playlists_audios_attributes = data.map do |like|
      bar.advance

      {
        audio_id: like.audio_id,
        playlist_id: playlists[like.user_id],
      }
    end

    PlaylistAudio.delete_all
    PlaylistAudio.upsert_all(playlists_audios_attributes) # rubocop:disable Rails/SkipsModelValidations

    ActiveRecord::Base.connection.execute <<-SQL.squish
      UPDATE playlists
      SET audios_count = (SELECT count(1) FROM playlist_audios WHERE playlist_audios.playlist_id = playlists.id)
    SQL
  end

  task archive: :environment do
    data = Importer::Upload.all.to_a
    bar = import_task_progressbar("archive", data.count)

    # When many audios linked to the same upload_id, take the first one
    audios = Audio.order(:title).pluck(Arel.sql("(settings ->> 'upload_id')::int as upload_id"), :id).group_by(&:shift).transform_values { |v| v.first&.first }
    job_args = []

    archives_attributes = data.map do |upload|
      job_args << [upload.id] unless upload.audio?
      bar.advance

      {
        audio_id: audios[upload.id],
        web_id: upload.web_id,
      }
    end

    Sidekiq.logger.level = Logger::WARN
    Importer::ArchiveUploadJob.perform_bulk(job_args)

    Archive.upsert_all(archives_attributes, unique_by: :web_id) # rubocop:disable Rails/SkipsModelValidations
  end

  private

  def import_task_progressbar(title, count)
    require "tty-progressbar"

    TTY::ProgressBar.new("#{title} [:bar] :current/:total :percent ET::elapsed ETA::eta", total: count, bar_format: :heart)
  end
end
