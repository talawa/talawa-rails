# frozen_string_literal: true

class AudioDashboard < BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    converted: Field::Boolean,
    description: Field::Text,
    disabled: Field::Boolean,
    human_duration: Field::Text,
    file: Field::ActiveStorage,
    image: Field::ActiveStorage,
    old_id: Field::Number,
    settings: Field::String.with_options(searchable: false),
    slug: Field::String,
    title: Field::String.with_options(truncate: 150),
    user: Field::BelongsTo,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    comments_count: Field::Number,
    likes_count: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    title
    human_duration
    converted
    disabled
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    id
    title
    slug
    user
    description
    comments_count
    likes_count
    human_duration
    file
    image
    settings
    converted
    disabled
    created_at
    updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    title
    description
    slug
    user
    file
    image
    disabled
  ].freeze

  COLLECTION_FILTERS = {
    converted: ->(resources) { resources.converted },
    disabled: ->(resources) { resources.where(disabled: true) },
  }.freeze

  def display_resource(audio)
    audio.title
  end
end
