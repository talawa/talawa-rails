# frozen_string_literal: true

require "rails_helper"

RSpec.describe Playlists::PlaylistsController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  let_it_be(:user) { create(:user) }

  describe "GET index" do
    subject { get :index, params: { user_id: user } }

    it "lists playlists" do
      subject
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET show" do
    subject { get :show, params: { user_id: user, id: playlist } }

    let(:playlist) { create(:playlist, user: user) }

    it "displays the playlist" do
      subject
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET new" do
    subject { get :new, params: { user_id: user } }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for a new playlist" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "GET edit" do
    subject { get :edit, params: { user_id: user, id: playlist } }

    let(:playlist) { create(:playlist, user: user) }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for editing the playlist" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "POST create" do
    subject { post :create, params: { user_id: user, playlist: attributes } }

    let(:attributes) { attributes_for(:playlist) }
    let(:playlist) { Playlist.last }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to created playlist" do
        subject
        expect(response).to redirect_to([user, playlist])
      end

      it "creates the playlist" do
        subject
        expect(playlist.title).to eq(attributes[:title])
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "PUT update" do
    subject { put :update, params: { user_id: user, id: playlist, playlist: { title: "New Title" } } }

    let(:playlist) { create(:playlist, user: user, title: "Title") }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to updated playlist" do
        subject
        expect(response).to redirect_to([playlist.user, playlist])
      end

      it "updates the playlist" do
        subject
        expect { playlist.reload }.to change(playlist, :title).from("Title").to("New Title")
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "DELETE destroy" do
    subject { delete :destroy, params: { user_id: user, id: playlist } }

    let!(:playlist) { create(:playlist, user: user) }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to user" do
        subject
        expect(response).to redirect_to(user_playlists_path(user))
      end

      it "destroys the playlist" do
        expect { subject }.to change(Playlist, :count).by(-1)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end
end
