# frozen_string_literal: true

require "rails_helper"

RSpec.describe Playlists::PlaylistsController do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/Talawa/playlists").to route_to("playlists/playlists#index", user_id: "Talawa")
    end

    it "routes to #new" do
      expect(get: "/Talawa/playlists/new").to route_to("playlists/playlists#new", user_id: "Talawa")
    end

    it "routes to #show" do
      expect(get: "/Talawa/playlists/new-playlist").to route_to("playlists/playlists#show", user_id: "Talawa", id: "new-playlist")
    end

    it "routes to #edit" do
      expect(get: "/Talawa/playlists/new-playlist/edit").to route_to("playlists/playlists#edit", user_id: "Talawa", id: "new-playlist")
    end

    it "routes to #create" do
      expect(post: "/Talawa/playlists").to route_to("playlists/playlists#create", user_id: "Talawa")
    end

    it "routes to #update via PUT" do
      expect(put: "/Talawa/playlists/new-playlist").to route_to("playlists/playlists#update", user_id: "Talawa", id: "new-playlist")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/Talawa/playlists/new-playlist").to route_to("playlists/playlists#update", user_id: "Talawa", id: "new-playlist")
    end

    it "routes to #destroy" do
      expect(delete: "/Talawa/playlists/new-playlist").to route_to("playlists/playlists#destroy", user_id: "Talawa", id: "new-playlist")
    end
  end
end
