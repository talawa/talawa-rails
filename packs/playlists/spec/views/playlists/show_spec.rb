# frozen_string_literal: true

require "rails_helper"

describe "playlists/playlists/show" do
  let_it_be(:user) { create(:user) }
  let_it_be(:playlist) { create(:playlist, user: user) }

  before { assign(:playlist, playlist) }

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
