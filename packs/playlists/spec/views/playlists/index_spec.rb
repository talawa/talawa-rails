# frozen_string_literal: true

require "rails_helper"

describe "playlists/playlists/index" do
  let_it_be(:user) { create(:user) }
  let_it_be(:playlists) { create_list(:playlist, 3, user: user) }

  before do
    assign(:user, user)
    assign(:playlists, playlists)
  end

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
