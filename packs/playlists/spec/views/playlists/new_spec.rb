# frozen_string_literal: true

require "rails_helper"

describe "playlists/playlists/new" do
  let_it_be(:user) { create(:user) }
  let_it_be(:playlist) { create(:playlist, user: user) }

  before do
    set_devise_mapping(context: @request) # rubocop:disable RSpec/InstanceVariable
    sign_in(user)
    assign(:playlist, playlist)
  end

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
