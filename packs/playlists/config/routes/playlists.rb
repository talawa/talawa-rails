# frozen_string_literal: true

resources :playlists, only: [:index, :show, :edit, :new, :create, :update, :destroy], controller: "playlists/playlists"
