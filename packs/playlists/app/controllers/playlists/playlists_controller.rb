# frozen_string_literal: true

module Playlists
  class PlaylistsController < ApplicationController
    include Pagy::Backend

    before_action :authenticate_user!, except: %i[index show]
    before_action :set_playlist, only: %i[show]
    before_action :set_user_playlist, only: %i[edit update destroy]

    def index
      records = user.playlists.includes(image_attachment: :blob).order(created_at: :desc)
      @pagy, @playlists = pagy_countless(records)
    end

    def show; end

    def new
      @playlist = current_user.playlists.new
    end

    def edit; end

    def create
      @playlist = current_user.playlists.new(playlist_params)
      @playlist.slug = @playlist.title&.parameterize

      if @playlist.save
        redirect_to [current_user, @playlist], notice: "Playlist was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @playlist.update(playlist_params)
        redirect_to [current_user, @playlist], notice: "Playlist was successfully updated.", status: :see_other
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @playlist.destroy!

      respond_to do |format|
        format.html { redirect_to user_playlists_path(current_user), notice: "Playlist was successfully destroyed.", status: :see_other }
        format.turbo_stream { flash[:notice] = "Playlist was successfully destroyed." } # rubocop:disable Rails/ActionControllerFlashBeforeRender
      end
    end

    private

    def user
      @user ||= User.find_by!(slug: params[:user_id])
    end

    def set_playlist
      @playlist = user.playlists.find_by!(slug: params[:id])
    end

    def set_user_playlist
      @playlist = current_user.playlists.find_by!(slug: params[:id])
    end

    def playlist_params
      params.require(:playlist).permit(:title, :image, :visibility)
    end
  end
end
