# frozen_string_literal: true

module Users
  class MigrateService < Service
    attr_reader :user, :password

    def initialize(user, password)
      @user = user
      @password = password
    end

    def perform
      return false unless user.settings.key?("imported_user") && user.settings["imported_user"]
      return false unless user.settings.key?("old_password_migrated") && !user.settings["old_password_migrated"]
      return false unless user.settings.key?("old_secret")

      user.password = password
      user.settings = user.settings.except("old_secret", "old_password", "old_password_migrated", "old_user_key")
      user.save

      true
    end
  end
end
