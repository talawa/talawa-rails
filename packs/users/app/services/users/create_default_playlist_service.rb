# frozen_string_literal: true

module Users
  class CreateDefaultPlaylistService < Service
    attr_reader :user, :title, :slug

    def initialize(user)
      @user = user
      @title = I18n.t("default_playlist")
      @slug = @title.parameterize
    end

    def perform
      return if user.playlists.where(locked: true).present?

      playlist = user.playlists.create(title: title, slug: slug, visibility: Playlist.visibilities[:open])
      playlist.update_columns(locked: true) # rubocop:disable Rails/SkipsModelValidations
    end
  end
end
