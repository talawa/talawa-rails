# frozen_string_literal: true

module Users
  class UsersController < ApplicationController
    include Pagy::Backend

    before_action :set_user, only: %i[show]

    def show
      @pagy, @audios = pagy_countless(Audio.user_feed(@user))
    end

    private

    def set_user
      @user = User.find_by!(slug: params[:id])
    end
  end
end
