# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::SessionsController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  describe "POST create" do
    context "given an imported user" do
      let(:user) { create(:user, :not_migrated) }

      it "calls Users::MigrateService service" do
        expect(Users::MigrateService).to receive(:perform).with(user, "password")
        post "create", params: { user: { email: user.email, password: "password" } }
      end

      context "given an invalid password" do
        it "does not call Users::MigrateService service" do
          expect(Users::MigrateService).not_to receive(:perform)
          post "create", params: { user: { email: user.email, password: "invalid" } }
        end
      end
    end

    context "given a regular user" do
      let(:user) { create(:user) }

      it "calls Users::MigrateService service" do
        expect(Users::MigrateService).to receive(:perform).with(user, "talawa")
        post "create", params: { user: { email: user.email, password: "talawa" } }
      end
    end
  end
end
