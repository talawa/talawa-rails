# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::UsersController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  let_it_be(:user) { create(:user) }

  describe "GET show" do
    subject { get :show, params: { id: user } }

    it "displays the user profile" do
      subject
      expect(response).to have_http_status(:ok)
    end
  end
end
