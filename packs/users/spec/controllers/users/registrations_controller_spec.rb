# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::RegistrationsController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  describe "POST create" do
    context "given valid attributes" do
      it "calls Users::CreateDefaultPlaylistService service" do
        expect(Users::CreateDefaultPlaylistService).to receive(:perform).with(instance_of(User))
        post "create", params: { user: attributes_for(:user) }
      end
    end

    context "given invalid attributes" do
      it "does not call Users::CreateDefaultPlaylistService service" do
        expect(Users::CreateDefaultPlaylistService).not_to receive(:perform)
        post "create", params: { user: {} }
      end
    end
  end

  describe "PUT update" do
    let(:user) { create(:user) }
    let(:title) { "New title" }

    before { sign_in(user) }

    it "does not need password for updates" do
      put "update", params: { user: { title: title } }
      expect(user.reload.title).to eq(title)
    end
  end
end
