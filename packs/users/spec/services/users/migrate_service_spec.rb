# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::MigrateService do
  let(:password) { SecureRandom.uuid }

  describe "#perform" do
    subject { described_class.new(user, password).perform }

    context "given a not migrated user" do
      let(:user) { create(:user, :not_migrated) }

      it "updates the user" do
        expect { subject }.to change(user, :updated_at)
      end

      it "updates the user's password" do
        subject
        expect(user.reload).to be_valid_password(password)
      end

      it "removes useless settings" do
        subject
        expect(user.reload.settings.keys).to match_array(%w[imported_user])
      end

      it "returns true" do
        expect(subject).to be_truthy
      end
    end

    context "given a migrated user" do
      let(:user) { create(:user, :imported) }

      it "updates the user's password" do
        subject
        expect(user.reload).to be_valid_password("talawa")
      end

      it "removes useless settings" do
        subject
        expect(user.settings.keys).not_to include(%w[old_secret old_password old_password_migrated old_user_key])
      end

      it "returns false" do
        expect(subject).to be_falsey
      end
    end
  end
end
