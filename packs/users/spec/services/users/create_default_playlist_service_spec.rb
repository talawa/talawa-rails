# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::CreateDefaultPlaylistService do
  let_it_be(:user) { create(:user) }

  describe "#perform" do
    subject { described_class.new(user).perform }

    it "creates a user's playlist" do
      expect { subject }.to change(user.playlists, :count).by(1)
    end

    it "gives a default playlist title" do
      subject
      expect(user.playlists.first.title).to eq(I18n.t("default_playlist"))
    end

    it "locks the playlist" do
      subject
      expect(user.playlists.first).to be_locked
    end

    it "gives open visibility to the playlist" do
      subject
      expect(user.playlists.first).to be_open
    end

    context "given an existing locked playlist" do
      before { create(:playlist, :locked, user: user, title: I18n.t("default_playlist")) }

      it "does not create a new user's playlist" do
        expect { subject }.not_to change(user.playlists, :count)
      end
    end
  end
end
