# frozen_string_literal: true

require "rails_helper"

describe "users/users/show" do
  let_it_be(:user) { create(:user) }

  before { assign(:user, user) }

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
