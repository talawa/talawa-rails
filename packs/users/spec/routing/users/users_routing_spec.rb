# frozen_string_literal: true

require "rails_helper"

RSpec.describe Users::UsersController do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/Talawa").to route_to("users/users#show", id: "Talawa")
    end
  end
end
