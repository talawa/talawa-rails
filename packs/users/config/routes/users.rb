# frozen_string_literal: true

devise_for :users, module: "users"
resources :users, only: [:show], path: "", controller: "users/users" do
  draw(:playlists)
  draw(:audios)
end
