# frozen_string_literal: true

resources :comments, only: [:edit, :new, :create, :update, :destroy], controller: "comments/comments"
