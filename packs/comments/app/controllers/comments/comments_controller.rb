# frozen_string_literal: true

module Comments
  class CommentsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_user_comment, only: %i[edit update destroy]

    def new
      render "_form"
    end

    def create
      @comment = Comment.new(comment_params)
      @comment.audio = audio
      @comment.user = current_user

      if @comment.save
        respond_to do |format|
          format.html { redirect_to [audio.user, audio], notice: "Comment was successfully created." }
          format.turbo_stream
        end
      else
        render "_form", status: :unprocessable_entity, locals: { comment: @comment }
      end
    end

    def edit
      render "_form"
    end

    def update
      if @comment.update(comment_params)
        redirect_to [audio.user, audio], notice: "Comment was successfully updated.", status: :see_other
      else
        render "_form", status: :unprocessable_entity, locals: { comment: @comment }
      end
    end

    def destroy
      @comment.destroy!

      respond_to do |format|
        format.html { redirect_to [audio.user, audio], notice: "Comment was successfully destroyed.", status: :see_other }
        format.turbo_stream { flash[:notice] = "Comment was successfully destroyed." } # rubocop:disable Rails/ActionControllerFlashBeforeRender
      end
    end

    private

    def comment_params
      params.require(:comment).permit(:body)
    end

    def audio
      @audio ||= Audio.find_by!(slug: params[:audio_id])
    end

    def set_user_comment
      @comment = audio.comments.find_by!(user: current_user, id: params[:id])
    end
  end
end
