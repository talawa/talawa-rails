# frozen_string_literal: true

require "rails_helper"

RSpec.describe Comments::CommentsController do
  describe "routing" do
    it "routes to #new" do
      expect(get: "/Talawa/new-audio/comments/new").to route_to("comments/comments#new", user_id: "Talawa", audio_id: "new-audio")
    end

    it "routes to #create" do
      expect(post: "/Talawa/new-audio/comments").to route_to("comments/comments#create", user_id: "Talawa", audio_id: "new-audio")
    end

    it "routes to #edit" do
      expect(get: "/Talawa/new-audio/comments/1/edit").to route_to("comments/comments#edit", user_id: "Talawa", audio_id: "new-audio", id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "/Talawa/new-audio/comments/1").to route_to("comments/comments#update", user_id: "Talawa", audio_id: "new-audio", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/Talawa/new-audio/comments/1").to route_to("comments/comments#update", user_id: "Talawa", audio_id: "new-audio", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/Talawa/new-audio/comments/1").to route_to("comments/comments#destroy", user_id: "Talawa", audio_id: "new-audio", id: "1")
    end
  end
end
