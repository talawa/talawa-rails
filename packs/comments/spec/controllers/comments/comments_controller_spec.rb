# frozen_string_literal: true

require "rails_helper"

RSpec.describe Comments::CommentsController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  let_it_be(:user) { create(:user) }
  let_it_be(:audio) { create(:audio) }

  describe "GET new" do
    subject { get :new, params: { user_id: user, audio_id: audio } }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for a new audio" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "GET edit" do
    subject { get :edit, params: { user_id: user, audio_id: audio, id: comment } }

    let(:comment) { create(:comment, user: user, audio: audio) }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for editing the comment" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "POST create" do
    subject { post :create, params: { user_id: user, audio_id: audio, comment: attributes } }

    let(:attributes) { attributes_for(:comment) }
    let(:comment) { Comment.last }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to the audio" do
        subject
        expect(response).to redirect_to([audio.user, audio])
      end

      it "creates the comment" do
        subject
        expect(comment.body).to eq(attributes[:body])
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "PUT update" do
    subject { put :update, params: { user_id: user, audio_id: audio, id: comment, comment: { body: "New body" } } }

    let(:comment) { create(:comment, user: user, audio: audio, body: "Body") }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to the audio" do
        subject
        expect(response).to redirect_to([audio.user, audio])
      end

      it "updates the comment" do
        subject
        expect { comment.reload }.to change(comment, :body).from("Body").to("New body")
      end
    end

    context "given another user signed in" do
      before { sign_in(audio.user) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "DELETE destroy" do
    subject { delete :destroy, params: { user_id: user, audio_id: audio, id: comment } }

    let!(:comment) { create(:comment, user: user, audio: audio) }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to the audio" do
        subject
        expect(response).to redirect_to([audio.user, audio])
      end

      it "destroys the comment" do
        expect { subject }.to change(Comment, :count).by(-1)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end
end
