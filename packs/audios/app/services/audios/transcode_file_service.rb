# frozen_string_literal: true

module Audios
  class TranscodeFileService < Service
    SILENCEREMOVE_OPTS = "silenceremove=start_periods=1:start_duration=0:start_threshold=0.02:detection=peak,areverse,silenceremove=start_periods=1:start_duration=0:start_threshold=0.02:detection=peak,areverse" # rubocop:disable Layout/LineLength
    LOUDNORM_OPTS = "loudnorm=I=-16:TP=-1.5:LRA=11"
    DEFAULT_OPTS = %w[-vn -map 0:a:0].freeze

    attr_reader :audio, :force, :blob

    def initialize(audio, force)
      @audio = audio
      @force = force
      @blob = audio.file.blob
    end

    def perform
      return unless blob&.audio?
      return if audio.converted? && !force

      download
      normalize
      convert
      strip_metadata
      replace

      audio.update(converted: true, duration: audio.file.metadata["duration"])
    ensure
      tmp_files.each_value do |file|
        unlink = true
        file.close(unlink)
      end
    end

    private

    def download
      output_file = tmp_files("blob")
      blob.open(tmpdir: Rails.root.join("tmp")) do |file|
        FileUtils.cp(file.path, output_file.path)
      end
    end

    def normalize
      # https://github.com/indiscipline/ffmpeg-loudnorm-helper/blob/main/src/main.rs
      # http://k.ylo.ph/2016/04/04/loudnorm.html
      input_file = tmp_files("blob")
      output_file = tmp_files("normalize")
      result = normalize_first_pass(input_file)
      normalize_second_pass(input_file, output_file, result)
    end

    def convert
      input_file = tmp_files("normalize")
      output_file = tmp_files("convert")
      transcode_options = DEFAULT_OPTS + %W[-c:a libfdk_aac -vbr 4 -movflags +faststart -af #{SILENCEREMOVE_OPTS}]
      media = FFMPEG::Media.new(input_file.path)
      media.transcode(output_file.path, transcode_options)
    end

    def strip_metadata
      input_file = tmp_files("convert")
      output_file = tmp_files("final")
      transcode_options = DEFAULT_OPTS + %w[-c:a copy -map_metadata -1 -map_chapters -1 -metadata:s handler_name=Talawa] # <3
      media = FFMPEG::Media.new(input_file.path)
      media.transcode(output_file.path, transcode_options)
    end

    def replace
      file = tmp_files("final")
      audio.file.purge
      audio.file.attach(io: File.open(file.path), filename: blob[:filename])
    end

    def tmp_files(name = nil)
      @tmp_files ||= {}
      return @tmp_files unless name

      @tmp_files[name] ||= Tempfile.new(%W[#{Rails.env}-#{name}-#{audio.id}- .m4a], Rails.root.join("tmp"))
    end

    def normalize_first_pass(file)
      transcode_options = DEFAULT_OPTS + %W[-af #{LOUDNORM_OPTS}:print_format=json -f null]
      media = FFMPEG::Media.new(file.path)
      transcoder = media.transcoder(File::NULL, transcode_options, validate: false)
      transcoder.run { ; } # rubocop:disable Lint/EmptyBlock
      MultiJson.load(transcoder.output.match(/{.+}/)[0])
    end

    def normalize_second_pass(input_file, output_file, result)
      i = result["input_i"]
      tp = result["input_tp"]
      lra = result["input_lra"]
      thresh = result["input_thresh"]
      offset = result["target_offset"]
      transcode_options = DEFAULT_OPTS + %W[-af #{LOUDNORM_OPTS}:measured_I=#{i}:measured_TP=#{tp}:measured_LRA=#{lra}:measured_thresh=#{thresh}:offset=#{offset}]
      media = FFMPEG::Media.new(input_file.path)
      media.transcode(output_file.path, transcode_options)
    end
  end
end
