# frozen_string_literal: true

module Audios
  class TranscodeFileJob < Job
    sidekiq_options queue: :convert,
                    retry: 0,
                    lock: :until_and_while_executing,
                    on_conflict: { client: :log, server: :reject }

    def perform(audio_id, force = false)
      audio = Audio.find(audio_id)

      Audios::TranscodeFileService.perform(audio, force)
    end
  end
end
