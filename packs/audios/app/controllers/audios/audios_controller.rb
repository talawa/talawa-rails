# frozen_string_literal: true

module Audios
  class AudiosController < ApplicationController
    include Pagy::Backend

    before_action :authenticate_user!, except: %i[feed show]
    before_action :set_audio, only: %i[show]
    before_action :set_user_audio, only: %i[edit update destroy]

    def feed
      @pagy, @audios = pagy_countless(Audio.feed)
    end

    def show
      @comments = @audio.comments.order(created_at: :desc).includes(user: [image_attachment: :blob])
    end

    def new
      @audio = current_user.audios.new
    end

    def edit; end

    def create
      @audio = current_user.audios.new(audio_params)
      @audio.slug = @audio.title&.parameterize

      if @audio.save
        Audios::TranscodeFileJob.perform_async(@audio.id)
        redirect_to [current_user, @audio], notice: "New audio has been enqueued for conversion. You will get a notification upon finished."
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @audio.update(audio_params)
        redirect_to [current_user, @audio], notice: "Audio was successfully updated.", status: :see_other
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @audio.destroy!

      respond_to do |format|
        format.html { redirect_to current_user, notice: "Audio was successfully destroyed.", status: :see_other }
        format.turbo_stream { flash[:notice] = "Audio was successfully destroyed." } # rubocop:disable Rails/ActionControllerFlashBeforeRender
      end
    end

    private

    def set_audio
      @audio = user.audios.find_by!(slug: params[:id])
    end

    def user
      @user ||= User.find_by!(slug: params[:user_id])
    end

    def set_user_audio
      @audio = current_user.audios.find_by!(slug: params[:id])
    end

    def audio_params
      params.require(:audio).permit(:title, :description, :image, :file)
    end
  end
end
