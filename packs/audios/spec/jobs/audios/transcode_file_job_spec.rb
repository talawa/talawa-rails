# frozen_string_literal: true

require "rails_helper"

RSpec.describe Audios::TranscodeFileJob do
  it { is_expected.to be_processed_in :convert }
  it { is_expected.to be_retryable 0 }

  describe "#perform" do
    subject { described_class.new }

    context "given an existing audio record" do
      let_it_be(:user) { create(:user) }
      let(:audio) { create(:audio, user: user) }

      it "runs successfully" do
        expect { subject.perform(audio.id) }.not_to raise_error
      end

      it "performs Audios::TranscodeFileService service" do
        expect(Audios::TranscodeFileService).to receive(:perform).once.with(audio, false)
        subject.perform(audio.id)
      end

      it "pass the force argument to service" do
        expect(Audios::TranscodeFileService).to receive(:perform).once.with(audio, true)
        subject.perform(audio.id, true)
      end
    end

    context "given an invalid audio record" do
      it "raises an exception" do
        expect { subject.perform(-1) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
