# frozen_string_literal: true

require "rails_helper"

describe "audios/audios/feed" do
  let_it_be(:user) { create(:user) }
  let_it_be(:audios) { create_list(:audio, 3, user: user) }

  before { assign(:audios, audios) }

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
