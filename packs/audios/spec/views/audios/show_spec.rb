# frozen_string_literal: true

require "rails_helper"

describe "audios/audios/show" do
  let_it_be(:user) { create(:user) }
  let_it_be(:audio) { create(:audio, user: user) }

  before { assign(:audio, audio) }

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
