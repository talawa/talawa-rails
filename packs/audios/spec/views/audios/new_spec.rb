# frozen_string_literal: true

require "rails_helper"

describe "audios/audios/new" do
  let_it_be(:user) { create(:user) }
  let_it_be(:audio) { create(:audio, user: user) }

  before do
    set_devise_mapping(context: @request) # rubocop:disable RSpec/InstanceVariable
    sign_in(user)
    assign(:audio, audio)
  end

  it "be successful" do
    render

    expect { rendered }.not_to raise_error
  end
end
