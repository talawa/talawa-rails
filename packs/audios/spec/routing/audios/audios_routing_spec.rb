# frozen_string_literal: true

require "rails_helper"

RSpec.describe Audios::AudiosController do
  describe "routing" do
    it "routes to #new" do
      expect(get: "/Talawa/audios/new").to route_to("audios/audios#new", user_id: "Talawa")
    end

    it "routes to #create" do
      expect(post: "/Talawa/audios").to route_to("audios/audios#create", user_id: "Talawa")
    end

    it "routes to #show" do
      expect(get: "/Talawa/new-audio").to route_to("audios/audios#show", user_id: "Talawa", id: "new-audio")
    end

    it "routes to #edit" do
      expect(get: "/Talawa/new-audio/edit").to route_to("audios/audios#edit", user_id: "Talawa", id: "new-audio")
    end

    it "routes to #update via PUT" do
      expect(put: "/Talawa/new-audio").to route_to("audios/audios#update", user_id: "Talawa", id: "new-audio")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/Talawa/new-audio").to route_to("audios/audios#update", user_id: "Talawa", id: "new-audio")
    end

    it "routes to #destroy" do
      expect(delete: "/Talawa/new-audio").to route_to("audios/audios#destroy", user_id: "Talawa", id: "new-audio")
    end
  end
end
