# frozen_string_literal: true

require "rails_helper"

RSpec.describe Audios::TranscodeFileService do
  let_it_be(:user) { create(:user) }

  describe "#perform" do
    subject { described_class.new(audio, force).perform }

    let(:force) { false }

    context "given an invalid audio record" do
      let(:audio) { build_stubbed(:audio, :with_file_invalid) }

      it "do nothing" do
        expect(subject).to be_nil
      end
    end

    context "given a valid audio record" do
      let(:audio) { create(:audio, user: user) }

      it "flags the audio as converted" do
        expect { subject }.to change(audio, :converted).from(false).to(true)
      end

      it "updates duration attribute" do
        expect { subject }.to change(audio, :duration).from(nil).to(4.439002)
      end

      it "trims silences in the audio file" do
        expect { subject }.to change { ActiveStorage::Analyzer::AudioAnalyzer.new(audio.file.blob).metadata[:duration] }.from(5.929796).to(4.439002)
      end

      it "deletes all temporary m4a files" do
        subject
        expect(Dir.glob(Rails.root.join("tmp/test-*.m4a").to_s).count).to eq(0)
      end
    end

    context "given a converted audio record" do
      let(:audio) { create(:audio, user: user, converted: true) }

      it "does not flag the audio as converted" do
        expect { subject }.not_to change(audio, :converted)
      end

      it "does not update duration attribute" do
        expect { subject }.not_to change(audio, :duration)
      end

      context "given force flag" do
        let(:force) { true }

        it "updates duration attribute" do
          expect { subject }.to change(audio, :duration).from(nil).to(4.439002)
        end
      end
    end
  end
end
