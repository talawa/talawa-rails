# frozen_string_literal: true

require "rails_helper"

RSpec.describe Audios::AudiosController do
  before { set_devise_mapping(context: @request) } # rubocop:disable RSpec/InstanceVariable

  let_it_be(:user) { create(:user) }

  describe "GET feed" do
    subject { get :feed }

    before { create_list(:audio, 3, user: user) }

    it "lists audios feed" do
      subject
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET show" do
    subject { get :show, params: { user_id: user, id: audio } }

    let(:audio) { create(:audio, user: user) }

    it "displays the audio" do
      subject
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET new" do
    subject { get :new, params: { user_id: user } }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for a new audio" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "GET edit" do
    subject { get :edit, params: { user_id: user, id: audio } }

    let(:audio) { create(:audio, user: user) }

    context "given user signed in" do
      before { sign_in(user) }

      it "displays the form for editing the audio" do
        subject
        expect(response).to have_http_status(:ok)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "POST create" do
    subject { post :create, params: { user_id: user, audio: attributes } }

    let(:attributes) { attributes_for(:audio) }
    let(:audio) { Audio.last }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to created audio" do
        subject
        expect(response).to redirect_to([user, audio])
      end

      it "creates the audio" do
        subject
        expect(audio.title).to eq(attributes[:title])
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "PUT update" do
    subject { put :update, params: { user_id: user, id: audio, audio: { title: "New Title" } } }

    let(:audio) { create(:audio, user: user, title: "Title") }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to updated audio" do
        subject
        expect(response).to redirect_to([audio.user, audio])
      end

      it "updates the audio" do
        subject
        expect { audio.reload }.to change(audio, :title).from("Title").to("New Title")
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe "DELETE destroy" do
    subject { delete :destroy, params: { user_id: user, id: audio } }

    let!(:audio) { create(:audio, user: user) }

    context "given user signed in" do
      before { sign_in(user) }

      it "redirects to user" do
        subject
        expect(response).to redirect_to(user)
      end

      it "destroys the audio" do
        expect { subject }.to change(Audio, :count).by(-1)
      end
    end

    context "given another user signed in" do
      before { sign_in(create(:user)) }

      it "raises ActiveRecord::RecordNotFound error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "given user signed out" do
      it "redirects to sign in path" do
        subject
        expect(response).to redirect_to(user_session_path)
      end
    end
  end
end
