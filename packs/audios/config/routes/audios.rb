# frozen_string_literal: true

resources :audios, only: [:new, :create], controller: "audios/audios"
resources :audios, only: [:show, :edit, :update, :destroy], path: "", controller: "audios/audios" do
  draw(:comments)
end
