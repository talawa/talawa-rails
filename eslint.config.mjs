import js from '@eslint/js';
import globals from 'globals';

export default [
  js.configs.recommended,
  {
    languageOptions: { globals: globals.browser }
  },
  {
    ignores: [
      '**/*.*',
      '!app/frontend/**/*.js',
      '!eslint.config.mjs',
    ],
  },
  {
    rules: {
      quotes: ['error', 'single', { 'avoidEscape': true }],
      'object-curly-spacing': ['error', 'always'],
    },
  }
];
