# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_07_25_141633) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "archives", id: false, force: :cascade do |t|
    t.bigint "audio_id"
    t.string "web_id", comment: "Old Talawa reference to support old links"
    t.index ["audio_id"], name: "index_archives_on_audio_id"
    t.index ["web_id"], name: "index_archives_on_web_id", unique: true
  end

  create_table "audios", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "old_id", comment: "Old talawa database uploads_files_id for migration"
    t.integer "comments_count", default: 0, null: false
    t.integer "likes_count", default: 0, null: false
    t.string "title", null: false
    t.string "slug", null: false
    t.text "description"
    t.float "duration"
    t.jsonb "settings", default: {}, null: false
    t.string "tags", default: [], array: true
    t.boolean "converted", default: false, null: false
    t.boolean "disabled", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["converted"], name: "index_audios_on_converted"
    t.index ["disabled"], name: "index_audios_on_disabled"
    t.index ["old_id"], name: "index_audios_on_old_id", unique: true, where: "(old_id IS NOT NULL)"
    t.index ["tags"], name: "index_audios_on_tags", using: :gin
    t.index ["user_id", "slug"], name: "index_audios_on_user_id_and_slug", unique: true
    t.index ["user_id"], name: "index_audios_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "audio_id", null: false
    t.bigint "parent_id"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["audio_id"], name: "index_comments_on_audio_id"
    t.index ["parent_id"], name: "index_comments_on_parent_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "likes", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "audio_id", null: false
    t.index ["audio_id"], name: "index_likes_on_audio_id"
    t.index ["user_id", "audio_id"], name: "index_likes_on_user_id_and_audio_id", unique: true
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "playlist_audios", force: :cascade do |t|
    t.bigint "playlist_id", null: false
    t.bigint "audio_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["audio_id"], name: "index_playlist_audios_on_audio_id"
    t.index ["playlist_id"], name: "index_playlist_audios_on_playlist_id"
  end

  create_table "playlists", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title", null: false
    t.string "slug", null: false
    t.text "description"
    t.integer "visibility", default: 0, null: false
    t.integer "audios_count", default: 0, null: false
    t.boolean "locked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locked"], name: "index_playlists_on_locked"
    t.index ["user_id", "slug"], name: "index_playlists_on_user_id_and_slug", unique: true
    t.index ["user_id"], name: "index_playlists_on_user_id"
    t.index ["visibility"], name: "index_playlists_on_visibility"
  end

  create_table "users", force: :cascade do |t|
    t.integer "old_id", comment: "Old talawa database user_id for migration"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "title", default: "", null: false
    t.string "slug", default: "", null: false
    t.integer "role", default: 0, null: false
    t.jsonb "settings", default: {}, null: false
    t.boolean "disabled", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "location"
    t.text "description", comment: "Text shown on user's profile"
    t.index ["disabled"], name: "index_users_on_disabled"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["old_id"], name: "index_users_on_old_id", unique: true, where: "(old_id IS NOT NULL)"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role"], name: "index_users_on_role"
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "archives", "audios"
  add_foreign_key "audios", "users"
  add_foreign_key "comments", "audios"
  add_foreign_key "comments", "comments", column: "parent_id"
  add_foreign_key "comments", "users"
  add_foreign_key "likes", "audios"
  add_foreign_key "likes", "users"
  add_foreign_key "playlist_audios", "audios"
  add_foreign_key "playlist_audios", "playlists"
  add_foreign_key "playlists", "users"
end
