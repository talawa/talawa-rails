class AddUserLocationAndDescription < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :location, :string
    add_column :users, :description, :text, comment: "Text shown on user's profile"
  end
end
