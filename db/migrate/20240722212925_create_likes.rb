class CreateLikes < ActiveRecord::Migration[7.1]
  def change
    create_table :likes, id: false do |t|
      t.references :user, null: false, foreign_key: true
      t.references :audio, null: false, foreign_key: true
    end

    add_index :likes, [:user_id, :audio_id], unique: true
  end
end
