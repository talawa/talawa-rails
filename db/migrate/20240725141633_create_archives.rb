class CreateArchives < ActiveRecord::Migration[7.1]
  def change
    create_table :archives, id: false do |t|
      t.references :audio, foreign_key: true
      t.string :web_id, comment: "Old Talawa reference to support old links"
    end

    add_index :archives, :web_id, unique: true
  end
end
