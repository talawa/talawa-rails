class CreateAudios < ActiveRecord::Migration[7.1]
  def change
    create_table :audios do |t|
      t.references :user, null: false, foreign_key: true

      t.integer :old_id, comment: "Old talawa database uploads_files_id for migration"
      t.integer :comments_count, null: false, default: 0
      t.integer :likes_count, null: false, default: 0

      t.string :title, null: false
      t.string :slug, null: false
      t.text :description
      t.float :duration

      t.jsonb :settings, null: false, default: {}
      t.string :tags, array: true, default: []
      t.boolean :converted, null: false, default: false
      t.boolean :disabled, null: false, default: false

      t.timestamps
    end

    add_index :audios, [:user_id, :slug], unique: true
    add_index :audios, :disabled
    add_index :audios, :converted
    add_index :audios, :tags, using: "gin"

    add_index :audios, :old_id, unique: true, where: "old_id IS NOT NULL"
  end
end
