class CreatePlaylists < ActiveRecord::Migration[7.1]
  def change
    create_table :playlists do |t|
      t.references :user, null: false, foreign_key: true

      t.string :title, null: false
      t.string :slug, null: false
      t.text :description
      t.integer :visibility, null: false, default: 0
      t.integer :audios_count, null: false, default: 0
      t.boolean :locked, null: false, default: false

      t.timestamps
    end

    add_index :playlists, :visibility
    add_index :playlists, :locked
    add_index :playlists, [:user_id, :slug], unique: true

    create_table :playlist_audios do |t|
      t.references :playlist, null: false, foreign_key: true
      t.references :audio, null: false, foreign_key: true

      t.timestamps
    end
  end
end
