# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.integer :old_id, comment: "Old talawa database user_id for migration"

      ## Database authenticatable
      t.string :email, null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string :current_sign_in_ip
      t.string :last_sign_in_ip

      t.string :title, null: false, default: ""
      t.string :slug, null: false, default: ""
      t.integer :role, null: false, default: 0
      t.jsonb :settings, null: false, default: {}
      t.boolean :disabled, null: false, default: false

      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :slug, unique: true
    add_index :users, :role
    add_index :users, :disabled

    add_index :users, :old_id, unique: true, where: "old_id IS NOT NULL"
  end
end
