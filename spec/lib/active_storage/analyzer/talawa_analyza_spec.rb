# frozen_string_literal: true

require "rails_helper"

RSpec.describe ActiveStorage::Analyzer::TalawaAnalyza do
  it { expect(described_class).to be < ActiveStorage::Analyzer::AudioAnalyzer }
  it { expect(described_class).not_to be_analyze_later }
end
