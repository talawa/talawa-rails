# frozen_string_literal: true

require "rails_helper"

RSpec.describe User do
  describe "#valid_password?" do
    subject { user.valid_password?(password) }

    context "given a valid password" do
      let(:password) { "talawa" }

      context "given an imported user" do
        let(:user) { build(:user, :imported) }

        it "returns true" do
          expect(subject).to be_truthy
        end
      end

      context "given a created user" do
        let(:user) { build(:user) }

        it "returns true" do
          expect(subject).to be_truthy
        end
      end

      context "given a not yet migrated user" do
        let(:password) { "password" }
        let(:user) { build(:user, :not_migrated) }

        it "returns true" do
          expect(subject).to be_truthy
        end
      end
    end

    context "given an invalid password" do
      let(:password) { "invalid" }

      context "given an imported user" do
        let(:user) { build(:user, :imported) }

        it "returns true" do
          expect(subject).to be_falsey
        end
      end

      context "given a created user" do
        let(:user) { build(:user) }

        it "returns true" do
          expect(subject).to be_falsey
        end
      end

      context "given a not yet migrated user" do
        let(:user) { build(:user, :not_migrated) }

        it "returns true" do
          expect(subject).to be_falsey
        end
      end
    end
  end

  describe "#role" do
    let(:roles) { [:user, :moderator, :admin] }

    it "has the right index" do
      roles.each_with_index do |role, index|
        expect(described_class.roles[role]).to eq(index)
      end
    end
  end

  describe "#email" do
    it "normalizes email" do
      user = build(:user, email: "Some@Email.com")
      expect(user.email).to eq("some@email.com")
    end

    it "strips email" do
      user = build(:user, email: " some@email.com ")
      expect(user.email).to eq("some@email.com")
    end
  end

  describe "#image" do
    context "given a valid image" do
      let(:user) { build(:user, :with_image) }

      it "be valid" do
        expect(user).to be_valid
      end

      it "returns no errors" do
        user.valid?
        expect(user.errors).to be_empty
      end

      it "calls image validator" do
        expect(user).to receive(:validate_image_content_type)
        user.valid?
      end
    end

    context "given an invalid image" do
      let(:user) { build(:user, :with_image_invalid) }

      it "does not be valid" do
        expect(user).not_to be_valid
      end

      it "returns invalid image error" do
        user.valid?
        expect(user.errors).to be_of_kind(:image, :invalid_content_type)
      end

      it "calls image validator" do
        expect(user).to receive(:validate_image_content_type)
        user.valid?
      end
    end

    context "given no image" do
      let(:user) { build(:user) }

      it "does not call image validator" do
        expect(user).not_to receive(:validate_image_content_type)
        user.valid?
      end
    end
  end

  describe "#title" do
    let(:user) { build(:user, title: title) }

    context "given an empty title" do
      let(:title) { nil }

      it "does not be valid" do
        expect(user).not_to be_valid
      end
    end

    context "given a short title" do
      let(:title) { "a" * (Rails.application.config.titles[:min_length] - 1) }

      it "does not be valid" do
        expect(user).not_to be_valid
      end
    end

    context "given a long title" do
      let(:title) { "a" * (Rails.application.config.titles[:max_length] + 1) }

      it "does not be valid" do
        expect(user).not_to be_valid
      end
    end

    context "given a title" do
      let(:title) { attributes_for(:user)[:title] }

      it "be valid" do
        expect(user).to be_valid
      end
    end
  end

  describe "#to_param" do
    context "given an user without a slug" do
      it "returns nil" do
        user = build(:user, slug: nil)

        expect(user.to_param).to be_nil
      end
    end
  end

  describe "#slug" do
    context "given an empty slug" do
      let(:user) { build(:user, slug: nil) }

      it "returns invalid slug error" do
        user.valid?
        expect(user.errors).to be_of_kind(:slug, :blank)
      end
    end

    it "be case sensitive" do
      create(:user, slug: "Talawa-User")
      user = build(:user, slug: "talawa-user")

      expect(user).not_to be_valid
    end
  end
end
