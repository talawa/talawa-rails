# frozen_string_literal: true

require "rails_helper"

RSpec.describe Playlist do
  let_it_be(:user) { create(:user) }

  describe "#audios_count" do
    subject { playlist.audios_count }

    context "given no attached audios" do
      let(:playlist) { build_stubbed(:playlist, user: user) }

      it "returns 0" do
        expect(subject).to eq(0)
      end
    end

    context "given one attached audio" do
      let(:playlist) { create(:playlist, :with_audios, user: user) }

      it "returns 1" do
        expect(subject).to eq(1)
      end

      context "given one more audio" do
        it "updates the counter cache" do
          playlist.playlist_audios << create(:playlist_audio)
          expect(subject).to eq(2)
        end
      end
    end
  end

  describe "#locked" do
    let!(:playlist) { create(:playlist, :locked, user: user) }

    context "given a locked playlist" do
      it "raises a ReadOnlyRecord exception on update" do
        expect { playlist.update(title: "Nope") }.to raise_error(ActiveRecord::ReadOnlyRecord)
      end

      it "raises a ReadOnlyRecord exception on delete" do
        expect { playlist.destroy }.to raise_error(ActiveRecord::ReadOnlyRecord)
      end

      context "given one more audio" do
        it "updates the counter cache" do
          playlist.playlist_audios << create(:playlist_audio, playlist: playlist)
          expect(playlist.audios_count).to eq(1)
        end
      end
    end
  end

  describe "#title" do
    context "given an empty title" do
      let(:playlist) { build(:playlist, user: user, title: nil) }

      it "returns invalid title error" do
        playlist.valid?
        expect(playlist.errors).to be_of_kind(:title, :blank)
      end
    end
  end

  describe "#to_param" do
    context "given a playlist without a slug" do
      it "returns nil" do
        playlist = build(:playlist, user: user, slug: nil)

        expect(playlist.to_param).to be_nil
      end
    end
  end

  describe "#slug" do
    context "given an empty slug" do
      let(:playlist) { build(:playlist, user: user, slug: nil) }

      it "returns invalid title error" do
        playlist.valid?
        expect(playlist.errors).to be_of_kind(:slug, :blank)
      end
    end

    it "be case sensitive" do
      create(:playlist, user: user, slug: "Talawa-Playlist")
      playlist = build(:playlist, user: user, slug: "talawa-playlist")

      expect(playlist).not_to be_valid
    end
  end
end
