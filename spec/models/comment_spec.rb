# frozen_string_literal: true

require "rails_helper"

RSpec.describe Comment do
  let_it_be(:user) { create(:user) }
  let_it_be(:audio) { create(:audio, user: user) }

  describe "#body" do
    let(:comment) { build(:comment, user: user, audio: audio, body: body) }

    context "given a valid body" do
      let(:body) { "This is the body" }

      it "be valid" do
        expect(comment).to be_valid
      end
    end

    context "given an empty body" do
      let(:body) { nil }

      it "returns invalid body error" do
        comment.valid?
        expect(comment.errors).to be_of_kind(:body, :blank)
      end
    end

    context "given a short body" do
      let(:body) { ":)" }

      it "returns invalid body error" do
        comment.valid?
        expect(comment.errors).to be_of_kind(:body, :too_short)
      end
    end
  end

  describe "#for_audio" do
    subject { described_class.for_audio(audio) }

    # Create a comment for an unrelated audio
    before { create(:comment, user: user) }

    let!(:comment_one) { create(:comment, user: user, audio: audio) }
    let!(:comment_two) { create(:comment, user: user, audio: audio) }
    let!(:reply_one) { create(:comment, user: user, audio: audio, parent: comment_one) }

    it "returns the comments tree" do
      expect(subject).to contain_exactly(comment_one, comment_two, reply_one)
    end
  end
end
