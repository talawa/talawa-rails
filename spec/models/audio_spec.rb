# frozen_string_literal: true

require "rails_helper"

RSpec.describe Audio do
  let_it_be(:user) { create(:user) }

  describe "#file" do
    context "given a valid file" do
      let(:audio) { build(:audio, user: user) }

      it "be valid" do
        expect(audio).to be_valid
      end

      it "returns no errors" do
        audio.valid?
        expect(audio.errors).to be_empty
      end

      it "calls file validator" do
        expect(audio).to receive(:validate_file_content_type)
        audio.valid?
      end
    end

    context "given an invalid audio" do
      let(:audio) { build(:audio, :with_file_invalid, user: user) }

      it "does not be valid" do
        expect(audio).not_to be_valid
      end

      it "returns invalid file error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:file, :invalid_content_type)
      end

      it "calls file validator" do
        expect(audio).to receive(:validate_file_content_type)
        audio.valid?
      end
    end

    context "given no file" do
      let(:audio) { build(:audio, :with_no_file, user: user) }

      it "does not call file validator" do
        expect(audio).not_to receive(:validate_file_content_type)
        audio.valid?
      end
    end
  end

  describe "#image" do
    context "given a valid image" do
      let(:audio) { build(:audio, :with_image, user: user) }

      it "be valid" do
        expect(audio).to be_valid
      end

      it "returns no errors" do
        audio.valid?
        expect(audio.errors).to be_empty
      end

      it "calls image validator" do
        expect(audio).to receive(:validate_image_content_type)
        audio.valid?
      end
    end

    context "given an invalid image" do
      let(:audio) { build(:audio, :with_image_invalid, user: user) }

      it "does not be valid" do
        expect(audio).not_to be_valid
      end

      it "returns invalid image error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:image, :invalid_content_type)
      end

      it "calls image validator" do
        expect(audio).to receive(:validate_image_content_type)
        audio.valid?
      end
    end

    context "given no image" do
      let(:audio) { build(:audio, user: user) }

      it "does not call image validator" do
        expect(audio).not_to receive(:validate_image_content_type)
        audio.valid?
      end
    end
  end

  describe "#tags" do
    let(:audio) { build(:audio, user: user) }

    context "given a valid list of tags" do
      it "be valid" do
        audio.tags = ["Jah Shakapunk", "King Quake II"]
        expect(audio).to be_valid
      end

      it "keeps the order of tags" do
        tags = %w[zzz aaa 999 111]
        audio.update(tags: tags)
        expect(audio.tags).to eq(tags)
      end
    end

    context "given too much tags" do
      before { audio.tags = SecureRandom.hex.scan(/.{1,4}/) }

      it "does not be valid" do
        expect(audio).not_to be_valid
      end

      it "returns invalid tags error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:tags, :too_long)
      end
    end

    context "given too short tags" do
      before { audio.tags = %w[iam so short] }

      it "does not be valid" do
        expect(audio).not_to be_valid
      end

      it "returns invalid tags error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:tags, :wrong_length)
      end
    end

    context "given too long tags" do
      before { audio.tags = [SecureRandom.hex * 3] }

      it "does not be valid" do
        expect(audio).not_to be_valid
      end

      it "returns invalid tags error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:tags, :wrong_length)
      end
    end
  end

  describe "#comments_count" do
    let(:audio) { create(:audio, :with_comments, user: user) }

    it "returns the count of associated comments" do
      expect(audio.comments_count).to eq(1)
    end
  end

  describe "#likes_count" do
    let(:audio) { create(:audio, :with_likes, user: user) }

    it "returns the count of associated likes" do
      expect(audio.likes_count).to eq(1)
    end
  end

  describe "#human_duration" do
    let(:audio) { build(:audio, user: user, duration: 5988.483) }

    it "returns the humanized duration" do
      expect(audio.human_duration).to eq("1 hour, 39 minutes, and 49 seconds")
    end

    context "given no duration" do
      let(:audio) { build(:audio, user: user) }

      it "returns no duration" do
        expect(audio.human_duration).to be_nil
      end
    end
  end

  describe "#to_param" do
    context "given an audio without a slug" do
      it "returns nil" do
        audio = build(:audio, user: user, slug: nil)

        expect(audio.to_param).to be_nil
      end
    end
  end

  describe "#slug" do
    context "given an empty slug" do
      let(:audio) { build(:audio, user: user, slug: nil) }

      it "returns invalid slug error" do
        audio.valid?
        expect(audio.errors).to be_of_kind(:slug, :blank)
      end
    end

    it "be case sensitive" do
      create(:audio, user: user, slug: "Talawa-Audio")
      audio = build(:audio, user: user, slug: "talawa-audio")

      expect(audio).not_to be_valid
    end
  end
end
