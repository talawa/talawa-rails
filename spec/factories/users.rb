# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    password { "talawa" }
    sequence(:title) { |n| "User ##{n}" }
    sequence(:slug) { |n| "user-#{n}" }

    settings do
      {
        imported_user: false,
      }
    end

    trait :imported do
      settings do
        {
          imported_user: true,
          old_password_migrated: true,
        }
      end
    end

    trait :not_migrated do
      settings do
        {
          imported_user: true,
          old_password_migrated: false,
          old_secret: "secret",
          old_password: "df7cddddc0e9b4966c93acb5317dc9ea030662a2", # hash of password "password"
          old_user_key: SecureRandom.uuid,
        }
      end
    end

    trait :with_image do
      image { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/user_avatar_valid.png"), "image/png") }
    end

    trait :with_image_invalid do
      image { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/user_avatar_invalid.txt"), "text/plain") }
    end
  end
end
