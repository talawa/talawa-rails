# frozen_string_literal: true

FactoryBot.define do
  factory :playlist do
    user
    sequence(:title) { |n| "Playlist ##{n}" }
    sequence(:slug) { |n| "playlist-#{n}" }

    trait :with_audios do
      after(:create) { |playlist| create(:playlist_audio, playlist: playlist) }
    end

    trait :locked do
      # FactoryBot can't create a readonly record directly, we need to bypass it
      after(:create) { |playlist| playlist.update_columns(locked: true) } # rubocop:disable Rails/SkipsModelValidations
    end
  end
end
