# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    user
    audio

    body { "body" }

    trait :with_reply do
      parent
    end
  end
end
