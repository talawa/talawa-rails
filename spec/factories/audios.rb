# frozen_string_literal: true

FactoryBot.define do
  factory :audio do
    user
    sequence(:title) { |n| "Audio ##{n}" }
    sequence(:slug) { |n| "audio-#{n}" }
    file { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/audio_file_valid.mp3"), "audio/mpeg") }

    trait :with_file_invalid do
      file { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/user_avatar_invalid.txt"), "text/plain") }
    end

    trait :with_no_file do
      file { nil }
    end

    trait :with_image do
      image { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/user_avatar_valid.png"), "image/png") }
    end

    trait :with_image_invalid do
      image { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/user_avatar_invalid.txt"), "text/plain") }
    end

    trait :with_comments do
      after(:create) { |audio| create(:comment, audio: audio) }
    end

    trait :with_likes do
      after(:create) { |audio| create(:like, audio: audio) }
    end
  end
end
