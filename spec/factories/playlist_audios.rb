# frozen_string_literal: true

FactoryBot.define do
  factory :playlist_audio do
    playlist
    audio
  end
end
