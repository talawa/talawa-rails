# frozen_string_literal: true

# https://github.com/mperham/sidekiq/wiki/Testing
require "sidekiq/testing"
