# frozen_string_literal: true

RSpec.configure do |config|
  unless ENV["CI"]
    config.before(:suite) do
      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.clean_with(:truncation)
    end

    config.around do |example|
      DatabaseCleaner.cleaning do
        example.run
      end
    end

    DatabaseCleaner.url_allowlist = [Rails.application.credentials.database_url]
  end
end
