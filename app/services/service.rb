# frozen_string_literal: true

class Service
  class << self
    def perform(...)
      ActiveSupport::ExecutionContext.set(service: to_s) do
        new(...).perform
      end
    end
  end
end
