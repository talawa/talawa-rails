import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  close() {
    if (this.element) {
      this.element.classList.add('transition', 'duration-500', 'ease-in-out', 'transform', 'opacity-0')
      setTimeout(() => this.element.remove(), 0.5 * 1000)
    }
  }
}
