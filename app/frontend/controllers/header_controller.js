import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  toggleDropdown() {
    const dropdown = document.getElementById('dropdown');
    dropdown.classList.toggle('hidden');

    const notifications = document.getElementById('notifications');
    notifications.classList.add('hidden');
  }
  toggleNotifications() {
    const notifications = document.getElementById('notifications');
    notifications.classList.toggle('hidden');

    const dropdown = document.getElementById('dropdown');
    dropdown.classList.add('hidden');
  }
}
