# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@talawa.fm"
  layout "mailer"
end
