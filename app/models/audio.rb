# frozen_string_literal: true

# == Schema Information
#
# Table name: audios
#
#  id                                                         :bigint           not null, primary key
#  comments_count                                             :integer          default(0), not null
#  converted                                                  :boolean          default(FALSE), not null
#  description                                                :text
#  disabled                                                   :boolean          default(FALSE), not null
#  duration                                                   :float
#  likes_count                                                :integer          default(0), not null
#  settings                                                   :jsonb            not null
#  slug                                                       :string           not null
#  tags                                                       :string           default([]), is an Array
#  title                                                      :string           not null
#  created_at                                                 :datetime         not null
#  updated_at                                                 :datetime         not null
#  old_id(Old talawa database uploads_files_id for migration) :integer
#  user_id                                                    :bigint           not null
#
# Indexes
#
#  index_audios_on_converted         (converted)
#  index_audios_on_disabled          (disabled)
#  index_audios_on_old_id            (old_id) UNIQUE WHERE (old_id IS NOT NULL)
#  index_audios_on_tags              (tags) USING gin
#  index_audios_on_user_id           (user_id)
#  index_audios_on_user_id_and_slug  (user_id,slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Audio < ApplicationRecord
  include Image
  include Slug
  include Title

  belongs_to :user

  has_many :comments, dependent: :delete_all
  has_many :likes, dependent: :delete_all
  has_one_attached :file

  taggable_array :tags

  scope :converted, -> { where(converted: true) }
  scope :feed, -> { where(disabled: false).includes(:user, image_attachment: :blob).order(created_at: :desc) }
  scope :user_feed, ->(user) { where(user: user).feed }

  validates :file, presence: true
  validate :validate_file_content_type, if: -> { attachment_changes["file"].present? && file.attached? }
  validates :tags, length: { maximum: Rails.application.config.tags[:max_count] }
  validate :validate_tags, if: -> { tags.present? && tags_changed? }

  def human_duration
    return if duration.blank?

    parts = ActiveSupport::Duration.build(duration).parts
    parts[:seconds] = parts[:seconds].ceil
    ActiveSupport::Duration.new(duration, parts).inspect
  end

  private

  def validate_file_content_type
    return if file.blob.blank?
    return if file.blob.content_type.blank?
    return if file.blob.audio?

    errors.add(:file, :invalid_content_type)
  end

  def validate_tags
    return if tags.all? { |e| e.length.between?(Rails.application.config.tags[:min_length], Rails.application.config.tags[:max_length]) }

    errors.add(:tags, :wrong_length, min: Rails.application.config.tags[:min_length], max: Rails.application.config.tags[:max_length])
  end
end
