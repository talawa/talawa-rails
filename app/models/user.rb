# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                                                :bigint           not null, primary key
#  current_sign_in_at                                :datetime
#  current_sign_in_ip                                :string
#  description(Text shown on user's profile)         :text
#  disabled                                          :boolean          default(FALSE), not null
#  email                                             :string           default(""), not null
#  encrypted_password                                :string           default(""), not null
#  last_sign_in_at                                   :datetime
#  last_sign_in_ip                                   :string
#  location                                          :string
#  remember_created_at                               :datetime
#  reset_password_sent_at                            :datetime
#  reset_password_token                              :string
#  role                                              :integer          default("user"), not null
#  settings                                          :jsonb            not null
#  sign_in_count                                     :integer          default(0), not null
#  slug                                              :string           default(""), not null
#  title                                             :string           default(""), not null
#  created_at                                        :datetime         not null
#  updated_at                                        :datetime         not null
#  old_id(Old talawa database user_id for migration) :integer
#
# Indexes
#
#  index_users_on_disabled              (disabled)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_old_id                (old_id) UNIQUE WHERE (old_id IS NOT NULL)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_role                  (role)
#  index_users_on_slug                  (slug) UNIQUE
#
class User < ApplicationRecord
  include Image
  include Slug
  include Title

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :trackable

  has_many :audios, dependent: :delete_all
  has_many :playlists, dependent: :delete_all

  enum :role, { user: 0, moderator: 1, admin: 2 }
  normalizes :email, with: ->(email) { email.strip.downcase }

  # TODO: Validate slug against restricted names (including /files)
  # validate :restricted_slugs, if: :title_changed?

  def remember_me
    super.nil? ? true : super
  end

  # Surcharge Devise#valid_password? method to make authentication works with imported user
  # Submitted valid passwords will be updated in user.password and old_password_migrated flag toggled
  def valid_password?(password)
    return super unless settings.key?("imported_user") && settings["imported_user"]
    return super unless settings.key?("old_password_migrated") && !settings["old_password_migrated"]
    return super unless settings.key?("old_secret")

    md5_secret = Digest::MD5.hexdigest(settings["old_secret"])
    sha1_secret = Digest::SHA1.hexdigest(settings["old_secret"])
    salt = Rails.application.credentials.old_website_password_salt
    hashed_password = Digest::SHA1.hexdigest("#{md5_secret}#{password}#{sha1_secret}#{salt}")
    settings["old_password"] == hashed_password
  end

  private

  def restricted_slugs
    path = ActionController::Routing::Routes.recognize_path("/#{slug}", method: :get)
    errors.add(:slug, "conflicts with existing path (/#{name})") if path && !path[:username]
  # rescue ActionController::RoutingError => _e
    # ignored
  end
end
