# frozen_string_literal: true

# == Schema Information
#
# Table name: playlists
#
#  id           :bigint           not null, primary key
#  audios_count :integer          default(0), not null
#  description  :text
#  locked       :boolean          default(FALSE), not null
#  slug         :string           not null
#  title        :string           not null
#  visibility   :integer          default("open"), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_playlists_on_locked            (locked)
#  index_playlists_on_user_id           (user_id)
#  index_playlists_on_user_id_and_slug  (user_id,slug) UNIQUE
#  index_playlists_on_visibility        (visibility)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Playlist < ApplicationRecord
  include Image
  include Slug
  include Title

  belongs_to :user
  has_many :playlist_audios, dependent: :delete_all
  has_many :audios, through: :playlist_audios

  # public? & private? are already taken by ruby itself :-(
  enum :visibility, { open: 0, privy: 1 }

  validates :title, presence: true

  def readonly?
    locked
  end
end
