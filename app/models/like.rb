# frozen_string_literal: true

# == Schema Information
#
# Table name: likes
#
#  audio_id :bigint           not null
#  user_id  :bigint           not null
#
# Indexes
#
#  index_likes_on_audio_id              (audio_id)
#  index_likes_on_user_id               (user_id)
#  index_likes_on_user_id_and_audio_id  (user_id,audio_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (audio_id => audios.id)
#  fk_rails_...  (user_id => users.id)
#
class Like < ApplicationRecord
  belongs_to :user
  belongs_to :audio, counter_cache: true
end
