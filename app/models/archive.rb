# frozen_string_literal: true

# == Schema Information
#
# Table name: archives
#
#  audio_id                                          :bigint
#  web_id(Old Talawa reference to support old links) :string
#
# Indexes
#
#  index_archives_on_audio_id  (audio_id)
#  index_archives_on_web_id    (web_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (audio_id => audios.id)
#
class Archive < ApplicationRecord
  belongs_to :audio, optional: true
end
