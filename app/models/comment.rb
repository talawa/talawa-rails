# frozen_string_literal: true

# == Schema Information
#
# Table name: comments
#
#  id         :bigint           not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  audio_id   :bigint           not null
#  parent_id  :bigint
#  user_id    :bigint           not null
#
# Indexes
#
#  index_comments_on_audio_id   (audio_id)
#  index_comments_on_parent_id  (parent_id)
#  index_comments_on_user_id    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (audio_id => audios.id)
#  fk_rails_...  (parent_id => comments.id)
#  fk_rails_...  (user_id => users.id)
#
class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :audio, counter_cache: true
  belongs_to :parent, class_name: "Comment", optional: true

  validates :body, presence: true, length: { minimum: Rails.application.config.comments[:min_length] }

  # TODO: Setup comment replies
  scope :for_audio, lambda { |audio|
    sql = <<-SQL.squish
      WITH RECURSIVE comment_tree AS (
          SELECT *, 0 AS level
          FROM comments
          WHERE parent_id IS NULL
          UNION ALL
          SELECT c.*, level + 1
          FROM comments c
          JOIN comment_tree p ON c.parent_id = p.id
      )
      SELECT * FROM comment_tree WHERE audio_id=? ORDER BY created_at
    SQL
    find_by_sql([sql, audio.id])
  }
end
