# frozen_string_literal: true

module Title
  extend ActiveSupport::Concern

  included do
    validates :title,
              presence: true,
              length: { minimum: Rails.application.config.titles[:min_length], maximum: Rails.application.config.titles[:max_length] }
  end
end
