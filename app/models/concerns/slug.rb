# frozen_string_literal: true

module Slug
  extend ActiveSupport::Concern

  included do
    validates :slug,
              presence: true,
              uniqueness: { case_sensitive: false, scope: name == "User" ? nil : :user_id },
              format: { with: /\A[a-zA-Z0-9-]+\z/ },
              length: { minimum: Rails.application.config.titles[:min_length], maximum: Rails.application.config.titles[:max_length] }

    def to_param
      slug
    end
  end
end
