# frozen_string_literal: true

module Image
  extend ActiveSupport::Concern

  included do
    has_one_attached :image do |attachable|
      attachable.variant(:thumb, resize_to_fill: [100, 100], format: :webp, saver: { strip: true })
      attachable.variant(:cover, resize_to_limit: [400, 400], format: :webp, saver: { strip: true })
    end

    validate :validate_image_content_type, if: -> { attachment_changes["image"].present? && image.attached? }

    private

    def validate_image_content_type
      return if image.blob.blank?
      return if image.blob.content_type.blank?
      return if image.blob.image?

      errors.add(:image, :invalid_content_type)
    end
  end
end
