# frozen_string_literal: true

# == Schema Information
#
# Table name: playlist_audios
#
#  id          :bigint           not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  audio_id    :bigint           not null
#  playlist_id :bigint           not null
#
# Indexes
#
#  index_playlist_audios_on_audio_id     (audio_id)
#  index_playlist_audios_on_playlist_id  (playlist_id)
#
# Foreign Keys
#
#  fk_rails_...  (audio_id => audios.id)
#  fk_rails_...  (playlist_id => playlists.id)
#
class PlaylistAudio < ApplicationRecord
  belongs_to :playlist, counter_cache: :audios_count
  belongs_to :audio
end
