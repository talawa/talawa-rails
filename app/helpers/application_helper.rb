# frozen_string_literal: true

module ApplicationHelper
  def flash_color_classes(type)
    case type
    when "success"
      "bg-green-100 border-green-400 text-green-700"
    when "alert"
      "bg-red-100 border-red-400 text-red-700"
    when "warning"
      "bg-yellow-100 border-yellow-400 text-yellow-700"
    when "notice"
      "bg-blue-100 border-blue-400 text-blue-700"
    else
      "bg-gray-100 border-gray-400 text-gray-700"
    end
  end

  def flash_color_cross(type)
    case type
    when "success"
      "text-green-500"
    when "alert"
      "text-red-500"
    when "warning"
      "text-yellow-500"
    when "notice"
      "text-blue-500"
    else
      "text-gray-500"
    end
  end
end
