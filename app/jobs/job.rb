# frozen_string_literal: true

class Job
  include Sidekiq::Job
end
