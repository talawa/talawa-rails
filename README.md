# TODO

- Delete user table old fields
- rsync -rav -e ssh --exclude=medias talawa:/data/web/talawa.fr/prod/users/ .
- Convert original avatar file

# Dev setup

- `git config core.hooksPath .githooks`

# Server setup

- install https://github.com/hyperupcall/autoenv
- git config credential.helper '!f() { echo username=gitlab-ci-token; echo "password=$GITLAB_TOKEN"; };f'
- echo "$GITLAB_TOKEN" | docker login registry.gitlab.com -u talawa --password-stdin
- To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
- https://github.com/Wowu/docker-rollout
- ip route add 172.24.0.0/16 dev talawa


# Sidekiq install

#!/bin/bash

for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
apt-get -yq update
apt-get -yq install ca-certificates curl
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get -yq update
apt-get -yq install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
systemctl enable docker
systemctl start docker
echo "$GITLAB_TOKEN" | docker login registry.gitlab.com -u talawa --password-stdin

apt-get -yq install wireguard
scp talawa:/tmp/talawa.conf /etc/wireguard/
systemctl enable wg-quick@talawa
systemctl start wg-quick@talawa

curl -#fLo- 'https://raw.githubusercontent.com/hyperupcall/autoenv/master/scripts/install.sh' | sh

apt-get -yq install sshfs
