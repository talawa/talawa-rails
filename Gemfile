# frozen_string_literal: true

source "https://rubygems.org"

gem "acts-as-taggable-array-on"
gem "administrate", "1.0.0.beta2"
gem "administrate-field-active_storage"
gem "bootsnap", require: false
gem "devise"
gem "dotenv"
gem "ffmpeg", git: "https://github.com/instructure/ruby-ffmpeg", require: false
gem "image_processing"
gem "mysql2"
gem "packs-rails"
gem "pagy"
gem "pg"
gem "puma"
gem "rails", "~> 7.1"
gem "rails_autolink"
gem "redis"
gem "sentry-rails"
gem "sentry-ruby"
gem "sentry-sidekiq"
gem "sidekiq"
gem "sidekiq-unique-jobs"
gem "stackprof"

# TODO: Removes those gems
gem "fog", require: false
gem "fog-internet-archive", require: false
gem "tty-progressbar", require: false

# Frontend gems
gem "sprockets-rails"
gem "turbo-rails"
gem "vite_rails"

group :development, :test do
  gem "factory_bot_rails"
  gem "rspec-rails"
  gem "rubocop", require: false
  gem "rubocop-capybara", require: false
  gem "rubocop-factory_bot", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "rubocop-rspec_rails", require: false
end

group :development do
  gem "annotate"
  gem "graphiql-rails"
  gem "web-console"
end

group :test do
  gem "database_cleaner-active_record"
  gem "rspec_junit_formatter", require: false
  gem "rspec-sidekiq"
  gem "simplecov", require: false
  gem "simplecov-cobertura", require: false
  gem "test-prof"
end
