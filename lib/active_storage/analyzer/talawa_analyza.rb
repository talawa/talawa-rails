# frozen_string_literal: true

module ActiveStorage
  class Analyzer
    class TalawaAnalyza < AudioAnalyzer
      def self.analyze_later?
        # Force ActiveStorage to analyze the file right away, not in a job
        false
      end
    end
  end
end
