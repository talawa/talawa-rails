# frozen_string_literal: true

# This is a monkeypatch to workaround this bug in the Fog-Rackspace gem:
# https://github.com/fog/fog-rackspace/issues/43

module Fog
  module Provider
    # Intercepts constant definition check to correct broken name in Fog gem
    def const_defined?(name, inherit = true) # rubocop:disable Style/OptionalBooleanParameter
      Object.const_defined? fixed_constant_name(name), inherit
    end

    # Intercepts constant get to correct broken name in Fog gem
    def const_get(name, inherit = true) # rubocop:disable Style/OptionalBooleanParameter
      Object.const_get fixed_constant_name(name), inherit
    end

    private

    # @param [String] name The requested constant name
    # @return [String] The corrected constant name
    def fixed_constant_name(name)
      return "Fog::Rackspace::CDNV2" if name == "Fog::Rackspace::CDN v2"
      return "Fog::Rackspace::ComputeV2" if name == "Fog::Rackspace::Compute v2"

      name
    end
  end
end
