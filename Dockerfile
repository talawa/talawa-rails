FROM jrottenberg/ffmpeg:7-alpine AS ffmpeg
FROM ruby:3.3.6-alpine AS dev

ARG USER_UID
ARG USER_GID
ARG RAILS_ROOT=/app
ARG PACKAGES="bash git nano build-base tzdata libpq-dev mariadb-connector-c-dev glib imagemagick libjpeg-turbo libxml2-dev curl-dev libwebp-tools nodejs npm"

WORKDIR $RAILS_ROOT

RUN addgroup --gid $USER_GID dev
RUN adduser --shell /bin/bash --uid $USER_UID --ingroup dev --disabled-password dev
RUN mkdir /usr/local/hist && chown -R $USER_GID:$USER_GID /usr/local/hist

RUN apk add --update --no-cache $PACKAGES

COPY --from=ffmpeg /bin/ffmpeg /bin/ffprobe /usr/local/bin/
COPY --from=ffmpeg /lib /usr/local/lib/

EXPOSE 3000
CMD ["/bin/bash"]

FROM ruby:3.3.6-alpine AS builder

ARG RAILS_ROOT=/app
ARG BUNDLE_EXTRA_GROUPS=""
ARG PACKAGES="git build-base libpq-dev mariadb-dev libxml2-dev curl-dev tzdata nodejs npm"

WORKDIR $RAILS_ROOT

RUN apk add --update --no-cache $PACKAGES

RUN bundle config set only default $BUNDLE_EXTRA_GROUPS

# Remove default gems
RUN gem list | grep -v "(default:" | cut -d" " -f1 | xargs gem uninstall -aIx -i /usr/local/lib/ruby/gems/3.3.0

COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs $(nproc) --retry 3
RUN find /usr/local/bundle/ -name "*.c" -delete \
    && find /usr/local/bundle/ -name "*.o" -delete \
    && find /usr/local/bundle/ -name "*.gem" -delete

COPY package.json package-lock.json ./
RUN npm install

COPY . ./
RUN cp .env.template .env
RUN RAILS_ENV=production rails assets:precompile --trace

FROM ruby:3.3.6-alpine

ARG RAILS_ROOT=/app
ARG BUNDLE_EXTRA_GROUPS=""
ARG PACKAGES="git tzdata libpq mariadb-connector-c glib imagemagick libjpeg-turbo libxml2 curl libwebp-tools"

WORKDIR $RAILS_ROOT

RUN apk add --update --no-cache $PACKAGES
RUN bundle config set only default $BUNDLE_EXTRA_GROUPS

COPY --from=ffmpeg /bin/ffmpeg /bin/ffprobe /usr/local/bin/
COPY --from=ffmpeg /lib /usr/local/lib/
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder /app/public/vite $RAILS_ROOT/public/vite/
COPY --from=builder /app/public/assets $RAILS_ROOT/public/assets/
COPY . $RAILS_ROOT

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
