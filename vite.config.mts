import { defineConfig } from 'vite'
import RubyPlugin from 'vite-plugin-ruby'
import FullReload from 'vite-plugin-full-reload'
import StimulusHMR from 'vite-plugin-stimulus-hmr'

export default defineConfig({
  plugins: [
    RubyPlugin(),
    StimulusHMR(),
    FullReload([
      'config/routes.rb',
      'app/views/**/*.erb',
      'packs/**/app/views/**/*.erb',
      'packs/**/config/routes/*.rb'
    ], { delay: 200 })
  ],
})
